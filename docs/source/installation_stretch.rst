.. _`installation`:

************
Installation sur Debian Stretch, en production ou développement.
************

**Prérequis**:

* Un serveur Debian Stretch 64 bits
* Un émulateur de terminal pour la connexion via ssh (natif sous Linux, `Putty <http://www.chiark.greenend.org.uk/~sgtatham/putty/>`_ ou `mobaXterm <https://mobaxterm.mobatek.net/>`_ sous windows)

Installation de l'environnement
===============================

Installation de PostgreSQL, de PostGIS et de l'environnement python
-------------------------------------------------------------------

Dans un terminal, lancez les commandes suivantes (si vous êtes déjà connecté en utilisateur **root**, supprimez l'instruction **sudo** :

.. code-block:: sh

    # Ajout des dépots officiels de PostgreSQL et PostGIS ainsi que Backports de Debian
    sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
    sudo sh -c 'echo "deb http://ftp.debian.org/debian $(lsb_release -cs)-backports main" > /etc/apt/sources.list.d/backports.list'
    sudo apt-get install wget ca-certificates
    wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
    # Mise à jour de la bibliothèque de paquets et mise à jour du système
    sudo apt-get update
    sudo apt-get upgrade
    # Installation des paquets requis pour dbchiroweb
    sudo apt-get install git python3 python3-pip virtualenv python3-virtualenv postgresql-10 postgresql-10-postgis-2.4 postgresql-10-postgis-2.4-scripts postgis
    # Installation des paquets requis pour le serveur apache
    sudo apt-get install python3-pip apache2 libapache2-mod-wsgi-py3
    # Installation des paquets requis pour la connexion httpS avec lets-encrypt
    sudo apt-get install python-certbot-apache -t stretch-backports

Clonage du dépot dbChiro[web]
=============================

C'est la méthode recommandée pour faciliter les mises à jour.

**dbchiroweb** sera installé dans le dossier ``/var/www/dbchiro``, il est conseiller de conserver cet emplacement sous peine de devoir adapter la suite de l'installation (chemins et configuration de apache2).

.. code-block:: sh

    # Création du dossier et clonage du projet
    mkdir /var/www/dbchiro
    cd /var/www/dbchiro
    git clone https://framagit.org/dbchiro/dbchiroweb.git

Création de l'environnement virtuel
===================================

La création d'un environnement virtuel Python3 dédié à **dbChiro** permet de s'affranchir d'éventuels conflits avec des librairies ou applications Python déjà présentes sur le serveur.

.. code-block:: sh

    # On crée l'environnement virtuel Python
    cd /var/www/dbchiro
    virtualenv -p python3 venv
    # On l'active 
    source venv/bin/activate
    # On installe les librairies nécessaires au fonctionnement de dbChiro
    pip install -r ./dbchiroweb/requirements.txt

Création de la base de donnée et du superutilisateur (administrateur) de la base de donnée
=========================================================================

.. code-block:: sh

    # A exécuter en tant que 'root', on bascule sur l'utilisateur 'postgres'
    su postgres
    # On ouvre la console SQL permettant de taper des instructions SQL
    psql

Exécuter les instructions SQL suivantes pour créer une base de données nommée **dbchiro** ainsi qu'un utilisateur **dbchiropguser** avec le mot de passe **motdepasse**. 

.. code:: psql

    CREATE DATABASE dbchirodb;
    CREATE ROLE dbchiropguser LOGIN SUPERUSER ENCRYPTED PASSWORD 'motdepasse';
    ALTER DATABASE dbchirodb OWNER TO dbchiropguser;

Tester la connexion à la base de donnée et activer PostGIS

.. code:: psql

    \c dbchirodb
    CREATE EXTENSION postgis;

Pour quitter la console SQL psql

.. code:: psql

    \q

Quitter l'utilisation **postgres** pour retourner avec **root** et continuer avec la configuration de l'application :

.. code-block:: sh

    su

Configuration de dbChiro[web]
=============================

Copiez le fichier ``dbchiro/settings/configuration/config.py.sample`` en le renommant ``config.py`` dans ce même dossier

.. code-block:: sh

    cd /var/www/dbchiro/dbchiroweb
    # Copie du fichier de configuration
    cp dbchiro/settings/configuration/config.py.sample dbchiro/settings/configuration/config.py
    # On édite le fichier
    nano dbchiro/settings/configuration/config.py

Le fichier de configuration ressemble à :

.. literalinclude:: ../../dbchiro/settings/configuration/config.py.sample
    :language: python

Pour faire fonctionner l'application, il est nécessaire de renseigner à minima :

* CUSTOM_HOSTS avec votre ou vos nom(s) de domaine,
* SECRET_KEY à générer en ligne,
* DB_NAME, le nom de votre bdd précédemment créee,
* DB_USER, le nom de l'utilisateur précédemment crée,
* DB_PWD, le mot de passe de l'utilisateur précédemment crée.

Il est également utile de renseigner les clé d'API pour la cartographie, ainsi que la position de focus (le centre de son territoire) et le niveau de zoom pour l'affichage des cartes dynamiques.


**La répartition des périodes est la suivante**

+-----------------------------------------+----------+-------------+-----------+----------+
|Jour de l'année                          | 335      | 60          | 136       | 228      |
+-----------------------------------------+----------+-------------+-----------+----------+
|date                                     | 01/12    | 01/03       | 16/05     | 16/08    |
+=========================================+==========+=============+===========+==========+
|Hivernage / wintering (w)                |     ≥    |     <       |           |          |
+-----------------------------------------+----------+-------------+-----------+----------+
|Transit printanier / sping transit (st)  |          |     ≥       |     <     |          |
+-----------------------------------------+----------+-------------+-----------+----------+
|Estivage / Summering (e)                 |          |             |     ≥     |    <     |
+-----------------------------------------+----------+-------------+-----------+----------+
|Transit automnal / Automn transit (ta)   |     <    |             |     ≥     |          |
+-----------------------------------------+----------+-------------+-----------+----------+


Initialisation de la base de données
====================================

Pour ce projet, deux configurations de ``settings`` sont possibles:

* ``production`` pour le serveur en production, qui force l'utilisation du httpS et désactive le mode ``DEBUG``,
* ``dev`` pour le développement, qui ne force pas le httpS et active le mode de débogage ainsi que l'appli django-debug-toolbar.

Les instructions ci-dessous permettent de tester l'application en mode ``dev``.  
Notez que la dernière instruction lance le serveur, mentionner l'adresse IP de votre serveur et le port 8000. Vous pourez donc tester l'application à l'adresse mentionnée, dans l'exemple : http://123.456.789.10:8000/

Si vous ne renseignez pas d'adresse et de port, l'application sera alors accessible sur le ``localhost`` (ou 127.0.0.1) du serveur. 
Pour la tester à distance, il faut utiliser un tunnel ssh entre la machine distante et la machine locale.
Sur Linux ou osX, cela se fait tout simplement avec la commande suivante (8000 est le port par défaut de django):

.. code-block:: sh

    ssh -p portssh utilisateurserver@adresseserver -L 8000:localhost:8000

Depuis Windows, cela peut se faire avec Putty: http://www.eila.univ-paris-diderot.fr/sysadmin/windows/tunnels-putty

.. code-block:: sh

    cd /var/www/dbchiro/dbchiroweb
    # On réactive l'environnement virtuel Python
    source ../venv/bin/activate
    # Création des fichiers d'initialisation de la base de donnée
    python manage.py makemigrations --settings=dbchiro.settings.dev accounts blog dicts geodata management sights
    # Création de la structure de la base de donnée (à partir des fichiers créées la commande
    python manage.py migrate --settings=dbchiro.settings.dev
    # Création du dossier 'static' et importation des fichiers nécessaire au fonctionnement de l'interface graphique (css, js, polices).
    python manage.py migrate --settings=dbchiro.settings.dev
    # Création du premier utilisateur, administrateur du site
    python manage.py createsuperuser --settings=dbchiro.settings.dev
    # Lancement du serveur
    python manage.py runserver 123.456.789.10:8000 --settings=dbchiro.settings.dev

Le terminal devrait alors renvoyer le résultat suivant:

.. code::

    Performing system checks...

    System check identified no issues (0 silenced).
    January 17, 2018 - 22:29:11
    Django version 1.11.3, using settings 'dbchiro.settings.dev'
    Starting development server at http://123.456.789.10:8000/
    Quit the server with CONTROL-C.

Les requêtes apparaitrons en temps réel dans le terminal. Cela permet de tester le bon fonctionnement de l'application avant de la placer en production.
Pour quitter, faire ``Ctrl+c``, Django stope alors le serveur et l'application n'est plus accessible. 
Si tout fonctionne en mode ``dev``, on peut alors passer en déploiement ``production``

.. Attention::
   Si vous reproduisez le code ci-dessus avec la configuration ``production``, vous obtiendrez sans doute des erreurs à cause du httpS. 
   En effet, ``runserver`` exécute un environnement de développement en http alors que la configuration ``production`` force le httpS.
   De plus, en configuration ``dev``, les fichiers statiques (css, js, images) sont servis par Django alors qu'en configuration ``production`` (quand DEBUG=False), c'est **apache2** qui doit prendre le relais. 
   Par défaut, ``wsgi.py`` utilise la configuration ``production``, la mise en production est détaillée plus bas. 


Intégration des données initiales (dictionnaires et geodata)
============================================================

L'étape suivante consiste à intégrer à la base de donnée les principales données nécessaires à son fonctionnemnt:
* les dictionnaires (application ``dicts``)
* les données géographiques (application ``geodata``)

Intégration des données de dictionnaire
---------------------------------------

Exécution du fichier `datas/sql/dicts_french.sql` avec la commande suivante :

.. code-block:: sh

    psql -h localhost -p 5432 -U dbchiropguser -W -f initial_data/sql/dicts_french.sql dbchirodb


Intégration des données cartographiques
---------------------------------------
Les données fournies sont en projection Lambert93 (epsg 2154). Il convient de les adapter à la projection de la base de donnée. Cela est réalisé par la commande d'importation suivante.
Plus d'informations sur cette commande ici: http://bostongis.com/pgsql2shp_shp2pgsql_quickguide.bqg

.. code-block:: sh

    cd /var/www/dbchiro/dbchiroweb
    shp2pgsql -s 2154:4326 -a datas/shapefiles/territory.shp public.geodata_territory | psql -h localhost -U dbchiropguser dbchirodb
    shp2pgsql -s 2154:4326 -a datas/shapefiles/municipality.shp public.geodata_municipality | psql -h localhost -p 5432 -U dbchiropguser dbchirodb



Déploiement de l'environnement en production
============================================================

Personnalisation du logo
-----------------------
Le logo du bandeau supérieur du site est à placer dans le dossier des fichiers statiques ``core/static/img/logo_site.png``. Le logo du projet est fourni par défaut. Pour l'installer, il suffit d’exécuter la commande suivante:

.. code-block:: sh

    cd /var/www/dbchiro/dbchiroweb
    cp core/static/img/logo_site.png.sample core/static/img/logo_site.png

Installation des fichiers statiques
-----------------------------------

Ce sont les fichiers ``*.css``, ``*.js`` et les images qui font le thème et le fonctionnement de l'interface graphique. pour les installer, il faut exécuter la commande suivante dans le dossier de l'application:

.. code-block:: sh

    cd /var/www/dbchiro/dbchiroweb
    source ../venv/bin/activate
    python ./manage.py collectstatic --settings=dbchiro.settings.dev

L'ensemble des fichiers statiques sont collectés dans le dossier ``static`` sous l'arborescence ``dbchiroweb``.

Préparation des fichiers médias (photos et pdf)
-----------------------------------

Création du dossier pour la réceptions des médias (photos et documents ).

.. code-block:: sh

    cd /var/www/dbchiro/dbchiroweb
    mkdir media
    chown -R www-data:www-data media


Configuration du serveur apache pour la production
-----------------------------------

Crééez le fichier de configuration du serveur apache. Cela par du principe que le domaine, ou sous-domaine, dispose d'une redirection vers l'adresse IP du présent serveur.
Plus d'informations à cette adresse: https://docs.djangoproject.com/fr/1.11/howto/deployment/wsgi/modwsgi/

.. code-block:: sh

    # Activation du module wsgi (normalement déjà activé)
    a2enmod wsgi
    # Copiez l'exemple de fichier de configuration apache pour dbChiro dans le dossier des sites disponibles d'apache et adaptez le à votre installation (remplacer ``mondomaine.org`` par votre nom de domaine et ``admin@dbchiro.org`` par votre adresse email).  
    cd /var/www/dbchiro/dbchiroweb
    cp dbchiro/settings/configuration/apache_dbchiro.conf.sample /etc/apache2/sites-available/dbchiro.conf

.. code-block:: sh

    # Editer le fichier 
     nano /etc/apache2/sites-available/dbchiro.conf

Voici à quoi doit ressembler le fichier.

.. code:: apache

    # Il est important de conserver WSGIDaemonProcess en dehors de <VirtualHost ...></VirtualHost> pour l'utilisation de letsencrypt
    WSGIDaemonProcess dbchiro python-home=/var/www/dbchiro/venv python-path=/var/www/dbchiro/dbchiroweb
    <VirtualHost mondomaine.org:80>
        ServerName mondomaine.org
        ServerAlias www.mondomaine.org
        ServerAdmin admin@dbchiro.org
        Alias /static /var/www/dbchiro/dbchiroweb/static
        Alias /media /var/www/dbchiro/dbchiroweb/media
        WSGIProcessGroup dbchiro
        WSGIScriptAlias / /var/www/dbchiro/dbchiroweb/dbchiro/wsgi.py
        <Directory /var/www/dbchiro/dbchiroweb/dbchiro>
            <files wsgi.py>
                Require all granted
            </files>
        </Directory>
        CustomLog /var/www/dbchiro/custom.log combined
        ErrorLog /var/www/dbchiro/error.log
    </VirtualHost>

Vérifier la syntaxe de votre configuration ``apache2``, qui doit retourner uniquement ``Syntax OK``, si ce n'est pas le cas, revoyez votre fichier :

.. code-block:: sh

    apache2ctl -t

Activez cette configuration avec la commande suivante:

.. code-block:: sh

    a2ensite dbchiro.conf
    service apache2 restart

Pour finir l'installation, il faut paramétrer le serveur pour l'utilisation de la connexion sécurisée httpS avec les certificat fournis par Let's Encrypt.

.. code-block:: sh

    certbot -d mondomaine.org

Suivez pas-à-pas l'installation : 
* votre mail, 
* accepter les condition d'utilisation,
* accepter ou refuser l'inscription à la liste d'info,
* ...
* choisir la certification de tous les domaines (mondomaine.org et www.mondomaine.org)

Lors de l'affichage des éléments suivants, choisissez l'option 2:

.. code-block:: none

    Please choose whether or not to redirect HTTP traffic to HTTPS, removing HTTP access.
    -------------------------------------------------------------------------------
    1: No redirect - Make no further changes to the webserver configuration.
    2: Redirect - Make all requests redirect to secure HTTPS access. Choose this for
    new sites, or if you're confident your site works on HTTPS. You can undo this
    change by editing your web server's configuration.
    -------------------------------------------------------------------------------

Une fois l'installation terminée, votre nom de domaine redirigera automatiquement vers une connexion chiffrée.

Pour renouveler automatiquement vos certificats, qui ont une durée de vie de 90jours, ajoutez la ligne suivante ``0 2 * * 1 /usr/bin/certbot renew >> /var/log/le-renew.log`` en bas de votre fichier de ``cron``.

Pour éditer le ``cron``, lancer :

.. code-block:: sh

    crontab -e

et choisir l'éditeur de fichier par défaut (``nano`` est le plus pratique).

Vous devriez maintenant accéder à votre plateforme depuis le nom de domaine renseigné.

Connectez vous avec votre compte d'administration précédemment créé et vérifier le bon fonctionnement de votre application.


