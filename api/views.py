from dal import autocomplete
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.serializers import serialize
from django.db.models import Q
from django.http import HttpResponse
from djgeojson.views import GeoJSONLayerView, TiledGeoJSONLayerView

from accounts.models import UserFullName
from dicts.models import Specie
from geodata.models import Territory, Municipality
from sights.models import Place


class ActiveUserAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return get_user_model().objects.none()

        qs = get_user_model().objects.filter(is_active=True).order_by('username')

        if self.q:
            qs = qs.filter(username__icontains=self.q)

        return qs


class AllUserAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return UserFullName.objects.none()

        qs = UserFullName.objects.all().order_by('username')

        if self.q:
            qs = qs.filter(Q(username__icontains=self.q) | Q(first_name__icontains=self.q) | Q(last_name__icontains=self.q))

        return qs


class TaxaAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return Specie.objects.none()

        qs = Specie.objects.all().order_by('-sp_true')

        if self.q:
            qs = qs.filter(Q(codesp__icontains=self.q)|Q(common_name_fr__icontains=self.q)|Q(sci_name__icontains=self.q))

        return qs


class MunicipalityAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        if not self.request.user.is_authenticated():
            return Municipality.objects.none()

        qs = Municipality.objects.all().order_by('code', 'name')

        if self.q:
            qs = qs.filter(name__icontains=self.q)

        return qs


class TerritoryAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        if not self.request.user.is_authenticated():
            return Territory.objects.none()

        qs = Territory.objects.all().order_by('code')

        if self.q:
            qs = qs.filter(name__icontains=self.q)

        return qs

class PlaceAutocomplete(LoginRequiredMixin, autocomplete.Select2QuerySetView):

    def get_queryset(self):
        loggeduser = self.request.user
        userdetail = get_user_model().objects.get(id=loggeduser.id)
        places = Place.objects.filter(telemetric_crossaz=False)
        if userdetail.access_all_data == True:
            print('Chargement de la carte pour l\'utilisateur avec accès total')
            qs = places
        elif userdetail.is_resp:
            print('Chargement de la carte pour le Coordinateur')
            resp_territory = userdetail.resp_territory.all()
            qs = places.filter(Q(territory__in=resp_territory) | (
                (~Q(territory__in=resp_territory) & (
                        Q(is_hidden=False) | (
                        Q(is_hidden=True) & (Q(created_by=loggeduser) | Q(authorized_user=loggeduser)))))))
        else:
            print('Chargement de la carte pour l\'obs lambda')
            qs = places.filter(
                Q(is_hidden=False) | (Q(is_hidden=True) & Q(authorized_user=loggeduser)))
        if self.q:
            qs = qs.filter(Q(name__icontains=self.q)|Q(territory__name__icontains=self.q)|Q(municipality__name__icontains=self.q)|Q(municipality__code__contains=self.q))
        return qs

class GeoJSONData(LoginRequiredMixin, GeoJSONLayerView):
    pass



# class GeoJSONPlaceData(LoginRequiredMixin, GeoJSONLayerView):
#     model = Place
#     properties = ('pk', 'name', 'type')
#     simplify = 0.5
#
#     def get_queryset(self):
#         loggeduser = self.request.user
#         user = get_user_model().objects.get(id=loggeduser.id)
#         if user.access_all_data == True:
#             print('Chargement de la carte pour l\'utilisateur avec accès total')
#             new_context = Place.objects.all()
#         elif user.is_resp:
#             print('Chargement de la carte pour le Coordinateur')
#             print(user.id)
#             print(loggeduser.id)
#             respterritory = user.resp_territory.all()
#             new_context = Place.objects.filter(Q(territory__in=respterritory) | (
#                 (~Q(territory__in=respterritory) & (
#                         Q(is_hidden=False) | (
#                         Q(is_hidden=True) & (Q(created_by=loggeduser) | Q(authorized_user=loggeduser)))))))
#         else:
#             print('Chargement de la carte pour l\'obs lambda')
#             new_context = Place.objects.filter(
#                 Q(is_hidden=False) | (Q(is_hidden=True) & Q(authorized_user__username__contains=loggeduser.username)))
#         return new_context


class TiledGeoJSONData(LoginRequiredMixin, TiledGeoJSONLayerView):
    pass

# class GeoJSONTiledPlaceData(TiledGeoJSONLayerView):
#     model = Place
#     properties = ('pk', 'name', 'type')


@login_required
def placeasgeojson(request):
    '''
    Vue permettant la génération d'un fichier geojson listant les localités que l'utilisateur est authorisé à voir

    :param request:
    :return:
    '''
    loggeduser = request.user
    userdetail = get_user_model().objects.get(id=loggeduser.id)
    places = Place.objects.filter(telemetric_crossaz=False)
    if userdetail.access_all_data == True:
        print('Chargement de la carte pour l\'utilisateur avec accès total')
        place_list = places
    elif userdetail.is_resp:
        print('Chargement de la carte pour le Coordinateur')
        resp_territory = userdetail.resp_territory.all()
        place_list = places.filter(Q(territory__in=resp_territory) | (
            (~Q(territory__in=resp_territory) & (
                    Q(is_hidden=False) | (
                    Q(is_hidden=True) & (Q(created_by=loggeduser) | Q(authorized_user=loggeduser)))))))
    else:
        print('Chargement de la carte pour l\'obs lambda')
        place_list = places.filter(
            Q(is_hidden=False) | (Q(is_hidden=True) & Q(authorized_user=loggeduser)))
    field = ('pk', 'name')
    geojson = serialize('geojson', place_list, geometry_field='geom', fields=field)
    return HttpResponse(geojson, content_type='application/json')


