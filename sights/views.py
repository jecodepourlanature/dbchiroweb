"""
	Vues de l'application Sights
"""
import datetime

from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.gis.geos import Point
from django.core.files.storage import FileSystemStorage
from django.db import IntegrityError
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.views.generic import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django_filters.views import FilterView
from django_tables2 import SingleTableView
from django_tables2.export import ExportMixin
from core import js

from dicts.models import CountPrecision
from .filters import PlaceFilter
from .forms import (CountDetailBiomForm, CountDetailOtherForm, CountDetailAcousticForm, CountDetailTelemetryForm,
                    BridgeForm, BuildForm, TreeForm, TreeGiteForm, CaveForm, PlaceForm, PlaceManagementForm,
                    SessionChangePlaceForm,
                    SessionForm, DeviceForm, SightingForm)
from .mixins import (AdvancedUserViewMixin, PlaceEditAuthMixin, PlaceViewAuthMixin, PlaceDetailEditAuthMixin,
                     PlaceDetailViewAuthMixin,
                     TreeGiteEditAuthMixin, SessionEditAuthMixin, SessionViewAuthMixin, SightingViewAuthMixin,
                     SightingEditAuthMixin,
                     SightingListAuthMixin, CountDetailEditAuthMixin)
from .models import (CountDetail, Place, PlaceManagement, Bridge,
                     Build, Tree, TreeGite, Cave, Session, Device, Sighting)
from .tables import (PlaceTable, PlaceManagementTable, PlaceSessionTable, BridgeTable, BuildTable, CaveTable, TreeTable,
                     TreeGiteTable, SessionTable, SessionDeviceTable, SessionSightingTable, SightingTable,
                     CountDetailBiomTable, CountDetailAcousticTable, CountDetailOtherTable, CountDetailTelemetryTable)

# from django_tables2.export.views import ExportMixin

IMAGE_FILE_TYPES = ['png', 'jpg', 'jpeg']
DOCUMENT_FILE_TYPES = ['doc', 'docx', 'odt', 'pdf']


class PlaceCreate(LoginRequiredMixin, CreateView):
    """Create view for the Place model."""

    model = Place
    form_class = PlaceForm
    file_storage = FileSystemStorage()
    template_name = 'leaflet_form.html'

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        if not form.instance.geom:
            if form.instance.x and form.instance.y:
                form.instance.geom = Point(form.instance.x, form.instance.y)
        else:
            form.instance.x = form.instance.geom.get_x()
            form.instance.y = form.instance.geom.get_y()
        if form.instance.type:
            if not form.instance.is_gite:
                form.instance.is_gite = True
        return super(PlaceCreate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(PlaceCreate, self).get_context_data(**kwargs)
        context['icon'] = 'fa fa-fw fa-map'
        context['title'] = _('Ajout d\'une localité')
        context['js'] = """   
        var dataurl = '/api/place.geojson';
        window.addEventListener("map:init", function (event) {
            var markers = L.markerClusterGroup({chunkedLoading: true, spiderfyDistanceMultiplier: 3});

            var style_data = {
                radius: 8,
                fillColor: "#ff4c41",
                color: "#ddd",
                weight: 2,
                opacity: 1,
                fillOpacity: 0.8
            };


            var map = event.detail.map;

            map.spin(true);

            // Fonction plein écran
            map.addControl(new L.Control.Fullscreen());


            // Géolocalisation de l'utilisateur

            var locateOptions = {
                flyTo: true,
                title: 'Ma position',
                icon: 'fa fa-location-arrow',
            };

            L.control.locate(locateOptions).addTo(map);

            // Recherche par adresse
            map.addControl(new L.Control.Search({
                url: 'https://nominatim.openstreetmap.org/search?format=json&accept-language=de-DE&q={s}',
                jsonpParam: 'json_callback',
                propertyName: 'display_name',
                propertyLoc: ['lat', 'lon'],
                markerLocation: true,
                autoType: true,
                autoCollapse: true,
                minLength: 2,
                zoom: 13,
                text: 'Suchen...',
                textCancel: 'Annuler',
                textErr: 'Erreur'
            }));

            // Download GeoJSON data with Ajax
            fetch(dataurl, {
                credentials: "include"
            })
                .then(function (resp) {
                    return resp.json();
                })
                .then(function (data) {
                    markers.addLayer(L.geoJson(data, {
                        onEachFeature: function onEachFeature(feature, layer) {
                            var props = feature.properties;
                            var content = `<a class="btn btn-primary btn-sm btn-block" align="center" style="color:white;" href="/place/${props.pk}/detail" target="_blank" style="font-color:white"><strong>${props.name}</strong></a>`;
                            layer.bindPopup(content);
                        }
                        ,
                        pointToLayer: function (feature, latlng) {
                            return L.circleMarker(latlng, style_data);
                        }
                    })).addTo(map);
                })
                .then(function () {
                    map.spin(false);
                });
                            
        });       
        """
        return context


class PlaceDetail(PlaceViewAuthMixin, DetailView):
    model = Place
    template_name = 'sights/place_detail.html'

    def get_context_data(self, **kwargs):
        context = super(PlaceDetail, self).get_context_data(**kwargs)
        loggeduser = self.request.user
        pk = self.kwargs.get('pk')
        placeobject = Place.objects.get(id_place=pk)
        placedetail = False
        #  Etats des lieux à afficher en fonction du type de localité (tree, build, bridge, cave)
        if placeobject.type:
            if placeobject.type.category in ('tree', 'building', 'bridge', 'cave'):
                placedetail = True
                if placeobject.type.category == 'tree':
                    placedetailcount = Tree.objects.filter(place=pk).count()
                    placedetailtable = TreeTable(Tree.objects.filter(place=pk))
                    placedetailicon = 'fa fa-tree'
                    placedetailtitle = _('Etats des lieux de l\'arbre')
                    placedetailcreateurl = 'sights:tree_create'
                elif placeobject.type.category == 'building':
                    placedetailcount = Build.objects.filter(place=pk).count()
                    placedetailtable = BuildTable(Build.objects.filter(place=pk))
                    placedetailicon = 'mki mki-ruins'
                    placedetailtitle = _('Etats des lieux du bâtiment')
                    placedetailcreateurl = '{% url \'sights:build_create\' pk=object.pk %}'
                elif placeobject.type.category == 'bridge':
                    placedetailcount = Bridge.objects.filter(place=pk).count()
                    placedetailtable = BridgeTable(Bridge.objects.filter(place=pk))
                    placedetailicon = 'mki mki-ruins'
                    placedetailtitle = _('Etats des lieux du pont')
                    placedetailcreateurl = '{% url \'sights:bridge_create\' pk=object.pk %}'
                elif placeobject.type.category == 'cave':
                    placedetailcount = Cave.objects.filter(place=pk).count()
                    placedetailtable = CaveTable(Cave.objects.filter(place=pk))
                    placedetailicon = 'mki mki-cave_entrance'
                    placedetailtitle = _('Etats des lieux de la cavité')
                    placedetailcreateurl = '{% url \'sights:cave_create\' pk=object.pk %}'
        # Données de session disponibles pour l'observateur
        if (get_user_model().objects.filter(id=loggeduser.id).filter(access_all_data=True)) or (
                get_user_model().objects.filter(id=loggeduser.id).filter(
                    is_resp=True) and placeobject.territory in get_user_model().objects.get(
            id=loggeduser.id).resp_territory.all()):
            # Si l'utilisateur a un accès total à toute la base
            sessioncount = Session.objects.filter(place=pk).count()
            sessiontable = PlaceSessionTable(Session.objects.filter(place=pk).order_by('-date_start'))
        elif placeobject in Place.objects.filter(is_hidden=False):
            # Si l'utilisateur est un simple observateur et que le site est non sensible, il peut en voir le détail et la
            # liste des sessions correspond aux sessions ou l'utilisateur est créateur ou observateur
            # TODO Gérer la cohérence entre les restrictions de vue de la localité et celles de la table des sessions
            print('%s | %s : site %s non caché' % (datetime.datetime.now().strftime("%Y%m%d %I%M%S"), loggeduser, pk))
            sessioncount = Session.objects.filter(Q(place=pk) & (
                    Q(created_by=loggeduser) | Q(main_observer=loggeduser) | Q(other_observer=loggeduser))).count()
            sessiontable = PlaceSessionTable(Session.objects.filter(Q(place=pk) & (
                    Q(created_by=loggeduser) | Q(main_observer=loggeduser) | Q(other_observer=loggeduser))))
        elif placeobject in Place.objects.filter(
                (Q(is_hidden=True) & Q(authorized_user__username__contains=loggeduser.username)) | (
                        Q(is_hidden=True) & Q(created_by=loggeduser))):
            # Si le site est caché, seul son créateur et les personnes autorisées peuvent le voir. la
            # liste des sessions correspond aux sessions ou l'utilisateur est créateur ou observateur
            # TODO Gérer la cohérence entre les restrictions de vue de la localité et celles de la table des sessions
            # Filtrage, l'utilisateur doit être créateur ou l'un des observateurs de la session
            sessioncount = Session.objects.filter(Q(place=pk) & (
                    Q(created_by=loggeduser) | Q(main_observer=loggeduser) | Q(other_observer=loggeduser))).count()
            sessiontable = PlaceSessionTable(Session.objects.filter(Q(place=pk) & (
                    Q(created_by=loggeduser) | Q(main_observer=loggeduser) | Q(other_observer=loggeduser))))
        context['icon'] = 'fa fa-fw fa-map'
        context['title'] = _('Détail d\'une localité')
        context['js'] = """      
        """
        context['sessionicon'] = 'fi-calendar'
        context['sessiontitle'] = _('Sessions d\'inventaires')
        context['sessioncount'] = sessioncount
        context['sessiontable'] = sessiontable
        context['placedetail'] = placedetail
        if placedetail:
            context['placedetailicon'] = placedetailicon
            context['placedetailtitle'] = placedetailtitle
            context['placedetailcount'] = placedetailcount
            context['placedetailtable'] = placedetailtable
            context['placedetailcreateurl'] = placedetailcreateurl
        if placeobject.is_managed:
            context['placemanagementicon'] = 'fi-checkbox'
            context['placemanagementtitle'] = _('Actions de gestion')
            context['placemanagementcount'] = PlaceManagement.objects.filter(place=pk).count()
            context['placemanagementtable'] = PlaceManagementTable(PlaceManagement.objects.filter(place=pk).order_by('-date'))
            context['placemanagementcreateurl'] = '{% url \'management:management_create\' pk=object.pk %}'
        return context


class PlaceUpdate(PlaceEditAuthMixin, UpdateView):
    model = Place
    form_class = PlaceForm
    file_storage = FileSystemStorage()
    template_name = 'leaflet_form.html'

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        if not form.instance.geom:
            if form.instance.x and form.instance.y:
                form.instance.geom = Point(form.instance.x, form.instance.y)
        else:
            form.instance.x = form.instance.geom.get_x()
            form.instance.y = form.instance.geom.get_y()
        if form.instance.type:
            if not form.instance.is_gite:
                form.instance.is_gite = True
        return super(PlaceUpdate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(PlaceUpdate, self).get_context_data(**kwargs)
        context['icon'] = 'fa fa-fw fa-map'
        context['title'] = 'Modification d\'une localité'
        context['js'] = """      
        """
        return context


class PlaceDelete(PlaceEditAuthMixin, DeleteView):
    model = Place
    template_name = 'confirm_delete.html'
    success_url = reverse_lazy('blog:home')

    def get_context_data(self, **kwargs):
        context = super(PlaceDelete, self).get_context_data(**kwargs)
        context['icon'] = 'fa fa-fw fa-trash'
        context['title'] = _('Suppression d\'une localité')
        context['message_alert'] = _('Êtes-vous certain de vouloir supprimer la localité')
        return context


class PlaceList(LoginRequiredMixin, FilterView, ExportMixin, SingleTableView):
    table_class = PlaceTable
    model = Place
    template_name = 'sights/place_search.html'
    table_pagination = {
        'per_page': 15
    }

    filterset_class = PlaceFilter

    def get_queryset(self):
        loggeduser = self.request.user
        user = get_user_model().objects.get(id=loggeduser.id)
        if user.access_all_data == True:
            print('Chargement de la carte pour l\'utilisateur avec accès total')
            new_context = Place.objects.all()
        elif user.is_resp:
            print('Chargement de la carte pour le Coordinateur')
            print(user.id)
            print(loggeduser.id)
            respterritory = user.resp_territory.all()
            new_context = Place.objects.filter(Q(territory__in=respterritory) | (
                (~Q(territory__in=respterritory) & (
                        Q(is_hidden=False) | (
                        Q(is_hidden=True) & (Q(created_by=loggeduser) | Q(authorized_user=loggeduser)))))))
        else:
            print('Chargement de la carte pour l\'obs lambda')
            new_context = Place.objects.filter(
                Q(is_hidden=False) | (Q(is_hidden=True) & Q(authorized_user__username__contains=loggeduser.username)))
        return new_context

    def get_context_data(self, **kwargs):
        context = super(PlaceList, self).get_context_data(**kwargs)
        context['icon'] = 'fa fa-fw fa-map'
        context['title'] = _('Rechercher une localité')
        context['createplacebtn'] = True
        context['js'] = """
        """
        return context


class PlaceMyList(LoginRequiredMixin, FilterView, ExportMixin, SingleTableView):
    table_class = PlaceTable
    model = Place
    template_name = 'sights/place_search.html'
    table_pagination = {
        'per_page': 15
    }

    filterset_class = PlaceFilter

    def get_queryset(self):
        loggeduser = self.request.user
        user = get_user_model().objects.get(id=loggeduser.id)
        new_context = Place.objects.filter(created_by=user)
        return new_context

    def get_context_data(self, **kwargs):
        context = super(PlaceMyList, self).get_context_data(**kwargs)
        context['icon'] = 'fa fa-fw fa-map'
        context['title'] = _('Rechercher dans mes localités')
        context['js'] = """
        """
        return context


class PlaceManagementCreate(LoginRequiredMixin, CreateView):
    """Create view for the Study model."""

    model = PlaceManagement
    form_class = PlaceManagementForm
    template_name = 'normal_form.html'

    def get_initial(self):
        initial = super(PlaceManagementCreate, self).get_initial()
        initial = initial.copy()
        initial['referent'] = self.request.user
        return initial

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        form.instance.place_id = self.kwargs.get('pk')
        return super(PlaceManagementCreate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(PlaceManagementCreate, self).get_context_data(**kwargs)
        context['icon'] = 'fi-star'
        context['title'] = _('Ajout d\'une action de gestion')
        context['js'] = js.DateInput
        return context


class PlaceManagementUpdate(PlaceDetailEditAuthMixin, UpdateView):
    model = PlaceManagement
    form_class = PlaceManagementForm
    template_name = 'normal_form.html'

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(PlaceManagementUpdate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(PlaceManagementUpdate, self).get_context_data(**kwargs)
        context['icon'] = 'fi-star'
        context['title'] = _('Mise à jour d\'une action de gestion')
        context['js'] = """      
        $(function () {
            $('.dateinput').fdatepicker({
                format: 'dd/mm/yyyy',
                disableDblClickSelection: true,
                leftArrow: '<i class="fa fa-fw fa-chevron-left"></i>',
                rightArrow: '<i class="fa fa-fw fa-chevron-right"></i>',
            });
        });
        """
        return context


class PlaceManagementDelete(PlaceDetailEditAuthMixin, DeleteView):
    model = PlaceManagement
    template_name = 'confirm_delete.html'
    success_url = reverse_lazy('management:study_list')

    def get_context_data(self, **kwargs):
        context = super(PlaceManagementDelete, self).get_context_data(**kwargs)
        context['icon'] = 'fa fa-fw fa-trash'
        context['title'] = _('Suppression d\'une action de gestion')
        context['message_alert'] = _('Êtes-vous certain de vouloir supprimer l\'action')
        return context


class SessionCreate(LoginRequiredMixin, CreateView):
    model = Session
    form_class = SessionForm
    template_name = 'normal_form.html'

    def get_initial(self):
        initial = super(SessionCreate, self).get_initial()
        initial = initial.copy()
        initial['main_observer'] = self.request.user
        return initial

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        form.instance.place_id = self.kwargs.get('pk')
        try:
            return super(SessionCreate, self).form_valid(form)
        except IntegrityError as e:
            messages.error(self.request, e.__cause__)
            return HttpResponseRedirect(self.request.path)
        # except IntegrityError as e:
        # # messages.error(self.request, e.__cause__)
        # messages.error(self.request, _('Cette session existe déjà'))
        # return HttpResponseRedirect(self.request.path)
        return super(SessionCreate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(SessionCreate, self).get_context_data(**kwargs)
        context['icon'] = 'fa fa-fw fa-map'
        context['title'] = _('Ajout d\'une session')
        # context['js'] = """
        # $(function () {
        #     $('.dateinput').fdatepicker({
        #         format: 'dd/mm/yyyy',
        #         disableDblClickSelection: true,
        #         leftArrow: '<i class="fa fa-fw fa-chevron-left"></i>',
        #         rightArrow: '<i class="fa fa-fw fa-chevron-right"></i>',
        #     });
        # });
        # """
        context['js'] = js.DateAndTimeInput
        return context


class SessionUpdate(SessionEditAuthMixin, UpdateView):
    model = Session
    form_class = SessionForm
    file_storage = FileSystemStorage()
    template_name = 'normal_form.html'

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(SessionUpdate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(SessionUpdate, self).get_context_data(**kwargs)
        context['icon'] = 'fi-calendar'
        context['title'] = _('Ajout d\'une session')
        context['js'] = js.DateAndTimeInput
        return context


class SessionChangePlaceUpdate(SessionEditAuthMixin, UpdateView):
    model = Session
    form_class = SessionChangePlaceForm
    file_storage = FileSystemStorage()
    template_name = 'normal_form.html'

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(SessionChangePlaceUpdate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(SessionChangePlaceUpdate, self).get_context_data(**kwargs)
        context['icon'] = 'fa fa-fw fa-exclamation-triangle'
        context['title'] = _('Changer la localité de la session')
        context['js'] = """      
        """
        return context


class SessionDelete(SessionEditAuthMixin, DeleteView):
    model = Session
    template_name = 'confirm_delete.html'

    def get_success_url(self):
        return reverse_lazy('sights:place_detail', kwargs={'pk': self.object.place_id})

    def get_context_data(self, **kwargs):
        context = super(SessionDelete, self).get_context_data(**kwargs)
        context['icon'] = 'fa fa-fw fa-trash'
        context['title'] = _('Suppression d\'une session')
        context['message_alert'] = _('Êtes-vous certain de vouloir supprimer la session')
        return context


class SessionDetail(SessionViewAuthMixin, DetailView):
    model = Session
    template_name = 'sights/session_detail.html'

    def get_context_data(self, **kwargs):
        context = super(SessionDetail, self).get_context_data(**kwargs)
        pk = self.kwargs.get('pk')
        # Rendu des sessions
        context['icon'] = 'fi-calendar'
        context['title'] = _('Détails de la session d\'inventaire')
        context['js'] = """      
        """
        # Rendu des observations
        context['sightingicon'] = 'fa fa-eye fa-fw'
        context['sightingtitle'] = _('Observations')
        context['sightingcount'] = Sighting.objects.filter(session=pk).distinct().count()
        context['sightingtable'] = SessionSightingTable(
            Sighting.objects.filter(session=pk).distinct().order_by('-timestamp_update'))
        # Rendu des dispositifs
        context['deviceicon'] = 'fi-target'
        context['devicetitle'] = _('Dispositifs d\'échantillonnage')
        context['devicecount'] = Device.objects.filter(session=pk).count()
        context['devicetable'] = SessionDeviceTable(Device.objects.filter(session=pk))
        return context


class SessionMyList(LoginRequiredMixin, SingleTableView):
    table_class = SessionTable
    template_name = 'table.html'
    table_pagination = {
        'per_page': 25
    }

    def get_context_data(self, **kwargs):
        context = super(SessionMyList, self).get_context_data(**kwargs)
        loggeduser = self.request.user
        context['icon'] = 'fi-calendar'
        context['title'] = _('Mes sessions')
        context['js'] = """
        """
        context['counttitle'] = _('Nombre de sessions')
        context['count'] = Session.objects.filter(Q(created_by=loggeduser) | Q(main_observer=loggeduser) | Q(
            other_observer__username__contains=loggeduser.username)).distinct().count()
        return context

    def get_queryset(self):
        loggeduser = self.request.user
        return Session.objects.filter(
            Q(created_by=loggeduser) | Q(
                main_observer=loggeduser) | Q(
                other_observer__username__contains=loggeduser.username)).distinct().order_by(
            '-timestamp_update')


class DeviceCreate(LoginRequiredMixin, CreateView):
    model = Device
    form_class = DeviceForm
    template_name = 'normal_form.html'

    # def get(self, request, *args, **kwargs):
    #     session = Session.objects.get(id_session=self.kwargs.get('pk'))
    #     self.form.fields['type'].queryset = TypeDevice.objects.filter(contact=session.contact.code)
    #     return super(DeviceCreate, self).get(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(DeviceCreate, self).get_form_kwargs()
        session_id = self.kwargs.get('pk')
        kwargs['contact'] = Session.objects.get(id_session=session_id).contact.code
        return kwargs

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        form.instance.session_id = self.kwargs.get('pk')
        return super(DeviceCreate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(DeviceCreate, self).get_context_data(**kwargs)
        context['icon'] = 'fi-target-two'
        context['title'] = _('Ajout d\'un dispositif')
        context['js'] = """      
        """
        return context


class DeviceUpdate(SightingEditAuthMixin, UpdateView):
    model = Device
    form_class = DeviceForm
    template_name = 'normal_form.html'

    def get_form_kwargs(self):
        kwargs = super(DeviceUpdate, self).get_form_kwargs()
        session_id = self.object.session_id
        kwargs['contact'] = Session.objects.get(id_session=session_id).contact.code
        return kwargs

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(DeviceUpdate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(DeviceUpdate, self).get_context_data(**kwargs)
        context['icon'] = 'fi-target-two'
        context['title'] = _('Modification d\'un dispositif')
        context['js'] = """      
        """
        return context


class DeviceDelete(SightingEditAuthMixin, DeleteView):
    model = Device
    template_name = 'confirm_delete.html'

    def get_success_url(self):
        return reverse_lazy('sights:session_detail', kwargs={'pk': self.object.session_id})

    def get_context_data(self, **kwargs):
        context = super(DeviceDelete, self).get_context_data(**kwargs)
        context['icon'] = 'fa fa-fw fa-trash'
        context['title'] = _('Suppression d\'une dispositif d\'échantillonage')
        context['message_alert'] = _('Êtes-vous certain de vouloir supprimer le dispositif')
        return context


class SightingCreate(LoginRequiredMixin, CreateView):
    model = Sighting
    form_class = SightingForm
    template_name = 'normal_form.html'

    def get_initial(self):
        initial = super(SightingCreate, self).get_initial()
        initial = initial.copy()
        initial['observer'] = self.request.user
        return initial

    def get_form_kwargs(self):
        kwargs = super(SightingCreate, self).get_form_kwargs()
        session_id = self.kwargs.get('pk')
        kwargs['session_id'] = session_id
        kwargs['contact'] = Session.objects.get(id_session=session_id).contact.code
        kwargs['main_observer'] = Session.objects.get(id_session=session_id).main_observer
        kwargs['other_observer'] = Session.objects.get(id_session=session_id).other_observer
        return kwargs

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        session_id = self.kwargs.get('pk')
        form.instance.session_id = session_id
        form.instance.observer_id = Session.objects.get(id_session=session_id).main_observer.id
        contact = Session.objects.get(id_session=session_id).contact.code
        if contact == 'du':
            #  Si le type de contact est détection acoustique, alors le 'total_count' vaut 1
            form.instance.total_count = 1
        try:
            return super(SightingCreate, self).form_valid(form)
        except IntegrityError as e:
            messages.error(self.request, e.__cause__)
            return HttpResponseRedirect(self.request.path)
        # except IntegrityError as e:
        #     # messages.error(self.request, e.__cause__)
        #     messages.error(self.request, _('Cette observation existe déjà'))
        #     return HttpResponseRedirect(self.request.path)
        return super(SightingCreate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(SightingCreate, self).get_context_data(**kwargs)
        context['icon'] = 'fa fa-eye fa-fw'
        context['title'] = 'Ajout d\'une observation'
        context['js'] = """      
        """
        return context

    def get_success_url(self):
        session_id = self.object.session_id
        if self.request.method == 'POST' and '_addanother' in self.request.POST:
            return reverse_lazy('sights:sighting_create', kwargs={'pk': session_id})
        return reverse_lazy('sights:session_detail', kwargs={'pk': session_id})


class SightingUpdate(SightingEditAuthMixin, UpdateView):
    model = Sighting
    form_class = SightingForm
    template_name = 'normal_form.html'

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        session_id = self.object.session_id
        form.instance.observer_id = Session.objects.get(id_session=session_id).main_observer.id
        contact = Session.objects.get(id_session=session_id).contact.code
        if contact == 'du':
            #  Si le type de contact est détection acoustique, alors le 'total_count' vaut 1
            form.instance.total_count = 1
        try:
            return super(SightingUpdate, self).form_valid(form)
        except IntegrityError as e:
            # messages.error(self.request, e.__cause__)
            messages.error(self.request, _('Cette observation existe déjà'))
            return HttpResponseRedirect(self.request.path)
        return super(SightingUpdate, self).form_valid(form)

    def get_form_kwargs(self):
        kwargs = super(SightingUpdate, self).get_form_kwargs()
        session_id = self.object.session_id
        kwargs['contact'] = Session.objects.get(id_session=session_id).contact.code
        kwargs['session_id'] = session_id
        kwargs['main_observer'] = Session.objects.get(id_session=session_id).main_observer
        kwargs['other_observer'] = Session.objects.get(id_session=session_id).other_observer
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(SightingUpdate, self).get_context_data(**kwargs)
        context['icon'] = 'fa fa-eye fa-fw'
        context['title'] = 'Modification d\'une observation'
        context['js'] = """      
        """
        return context


class SightingDelete(SightingEditAuthMixin, DeleteView):
    model = Sighting
    template_name = 'confirm_delete.html'

    def get_success_url(self):
        return reverse_lazy('sights:session_detail', kwargs={'pk': self.object.session_id})

    def get_context_data(self, **kwargs):
        context = super(SightingDelete, self).get_context_data(**kwargs)
        context['icon'] = 'fa fa-fw fa-trash'
        context['title'] = _('Suppression d\'une observation')
        context['message_alert'] = _('Êtes-vous certain de vouloir supprimer l\'observation')
        return context


class SightingDetail(SightingViewAuthMixin, DetailView):
    model = Sighting
    template_name = 'sights/sighting_detail.html'

    def get_context_data(self, **kwargs):
        context = super(SightingDetail, self).get_context_data(**kwargs)
        pk = self.kwargs.get('pk')
        # Rendu des sessions
        context['icon'] = 'fa fa-eye fa-fw'
        context['title'] = _('Détail d\'une observation')
        context['js'] = """      
        """
        # Rendu des observations
        context['countdetailicon'] = 'fa fa-eye fa-fw'
        context['countdetailtitle'] = _('Détails de l\'observation')
        context['countdetailcount'] = CountDetail.objects.filter(sighting=pk).distinct().count()
        if Sighting.objects.get(id_sighting=pk).session.contact.code:
            contact = Sighting.objects.get(id_sighting=pk).session.contact.code
            context['contact'] = contact
        else:
            contact = 'nc'
        if contact in ('vm', 'ca'):
            countdetailtable = CountDetailBiomTable(CountDetail.objects.filter(sighting=pk))
        elif contact == 'du':
            countdetailtable = CountDetailAcousticTable(CountDetail.objects.filter(sighting=pk))
        elif contact == 'te':
            countdetailtable = CountDetailTelemetryTable(CountDetail.objects.filter(sighting=pk))
        else:
            countdetailtable = CountDetailOtherTable(CountDetail.objects.filter(sighting=pk))
        context['countdetailtable'] = countdetailtable
        # Rendu des dispositifs
        context['deviceicon'] = 'fi-target'
        context['devicetitle'] = _('Sessions d\'inventaires')
        context['devicecount'] = Device.objects.filter(session=pk).count()
        context['devicetable'] = SessionDeviceTable(Device.objects.filter(session=pk))
        return context


class SightingList(SightingListAuthMixin, ExportMixin, SingleTableView):
    table_class = SightingTable
    template_name = 'table.html'
    table_pagination = {
        'per_page': 25
    }
    export_name = _('observations')

    def get_context_data(self, **kwargs):
        context = super(SightingList, self).get_context_data(**kwargs)
        loggeduser = get_user_model().objects.get(id=self.request.user.id)
        context['icon'] = 'fa fa-eye fa-fw'
        context['title'] = _('Liste des observations')
        context['export'] = False
        context['js'] = """
        """
        context['counttitle'] = _('Nombre d\'observations')
        if loggeduser.access_all_data or loggeduser.edit_all_data:
            count = Sighting.objects.all().distinct().count()
        elif loggeduser.is_resp:
            count = Sighting.objects.filter(Q(created_by=loggeduser) | Q(observer=loggeduser) | Q(
                session__other_observer__username__contains=loggeduser.username) | Q(
                session__place__territory__in=loggeduser.resp_territory.all())).count()
        context['count'] = count
        return context

    def get_queryset(self):
        loggeduser = get_user_model().objects.get(id=self.request.user.id)
        if loggeduser.access_all_data or loggeduser.edit_all_data:
            return Sighting.objects.all().distinct().order_by(
                '-timestamp_update')
        elif loggeduser.is_resp:
            return Sighting.objects.filter(Q(created_by=loggeduser) | Q(
                observer=loggeduser) | Q(
                session__other_observer__username__contains=loggeduser.username) | Q(
                session__place__territory__in=loggeduser.resp_territory.all())
                                           ).distinct().order_by(
                '-timestamp_update')


class SightingMyList(LoginRequiredMixin, ExportMixin, SingleTableView):
    table_class = SightingTable
    template_name = 'table.html'
    table_pagination = {
        'per_page': 25
    }
    export_name = _('mes_observations')

    def get_context_data(self, **kwargs):
        context = super(SightingMyList, self).get_context_data(**kwargs)
        loggeduser = get_user_model().objects.get(id=self.request.user.id)
        context['icon'] = 'fa fa-eye fa-fw'
        context['title'] = _('Mes observations')
        context['js'] = """
        """
        context['counttitle'] = _('Nombre d\'observations')
        context['export'] = True
        context['count'] = Sighting.objects.filter(
            Q(created_by=loggeduser) | Q(
                observer=loggeduser) | Q(
                session__other_observer__username__contains=loggeduser.username)).distinct().count()
        return context

    def get_queryset(self):
        loggeduser = self.request.user
        return Sighting.objects.filter(
            Q(created_by=loggeduser) | Q(
                observer=loggeduser) | Q(
                session__other_observer__username__contains=loggeduser.username)).distinct().order_by(
            '-timestamp_update')


class SightingUserList(AdvancedUserViewMixin, SingleTableView):
    table_class = SightingTable
    template_name = 'table.html'
    table_pagination = {
        'per_page': 25
    }
    export_name = 'export_user_sighting.ext'

    def get_context_data(self, **kwargs):
        context = super(SightingUserList, self).get_context_data(**kwargs)
        user = get_user_model().objects.get(id=self.kwargs.get('pk'))
        loggeduser = get_user_model().objects.get(id=self.request.user.id)
        context['icon'] = 'fa fa-eye fa-fw'
        context['title'] = _('Observations de ') + user.get_full_name()
        context['js'] = """
        """
        context['counttitle'] = _('Nombre d\'observations')
        context['export'] = True
        userlist = Sighting.objects.filter(
            Q(created_by=user) | Q(
                observer=user) | Q(
                session__other_observer__username__contains=user.username))
        if loggeduser.access_all_data or loggeduser.edit_all_data:
            context['count'] = userlist.distinct().count()
        elif loggeduser.is_resp:
            context['count'] = userlist.filter(
                session__place__territory__in=loggeduser.resp_territory.all()).distinct().count()
        return context

    def get_queryset(self):
        user = get_user_model().objects.get(id=self.kwargs.get('pk'))
        loggeduser = get_user_model().objects.get(id=self.request.user.id)
        userlist = Sighting.objects.filter(
            Q(created_by=user) | Q(
                observer=user) | Q(
                session__other_observer__username__contains=user.username))
        if loggeduser.access_all_data or loggeduser.edit_all_data:
            return userlist.distinct().order_by(
                '-timestamp_update')
        elif loggeduser.is_resp:
            return userlist.filter(session__place__territory__in=loggeduser.resp_territory.all()).distinct().order_by(
                '-timestamp_update')


#################################################
#               Build model views                #
#################################################

class BuildCreate(LoginRequiredMixin, CreateView):
    model = Build
    form_class = BuildForm
    template_name = 'normal_form.html'

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        form.instance.place_id = self.kwargs.get('pk')
        try:
            return super(BuildCreate, self).form_valid(form)
        except IntegrityError as e:
            # messages.error(self.request, e.__cause__)
            messages.error(self.request, _('Cette état des lieux existe déjà'))
            return HttpResponseRedirect(self.request.path)
        return super(BuildCreate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(BuildCreate, self).get_context_data(**kwargs)
        context['icon'] = 'mki mki-ruins'
        context['title'] = _('Ajout d\'un état des lieux de bâtiment')
        context['js'] = js.DateInput
        return context


class BuildUpdate(PlaceDetailEditAuthMixin, UpdateView):
    model = Build
    form_class = BuildForm
    template_name = 'normal_form.html'

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        try:
            return super(BuildUpdate, self).form_valid(form)
        except IntegrityError as e:
            # messages.error(self.request, e.__cause__)
            messages.error(self.request, _('Cette état des lieux existe déjà'))
            return HttpResponseRedirect(self.request.path)
        return super(BuildUpdate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(BuildUpdate, self).get_context_data(**kwargs)
        context['icon'] = 'mki mki-ruins'
        context['title'] = _('Modification d\'un état des lieux de bâtiment')
        context['js'] = js.DateInput
        return context


class BuildDelete(PlaceDetailEditAuthMixin, DeleteView):
    model = Build
    template_name = 'confirm_delete.html'

    def get_success_url(self):
        return reverse_lazy('sights:place_detail', kwargs={'pk': self.object.place_id})

    def get_context_data(self, **kwargs):
        context = super(PlaceBuildDetailDelete, self).get_context_data(**kwargs)
        context['icon'] = 'fa fa-fw fa-trash'
        context['title'] = _('Suppression d\'un état des lieux de bâtiment')
        context['message_alert'] = _('Êtes-vous certain de vouloir supprimer l\'état des lieux de bâtiment')
        return context


#################################################
#               Tree model views                #
#################################################


class TreeCreate(LoginRequiredMixin, CreateView):
    model = Tree
    form_class = TreeForm
    template_name = 'normal_form.html'

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        form.instance.place_id = self.kwargs.get('pk')
        try:
            return super(TreeCreate, self).form_valid(form)
        except IntegrityError as e:
            # messages.error(self.request, e.__cause__)
            messages.error(self.request, _('Cette état des lieux existe déjà'))
            return HttpResponseRedirect(self.request.path)
        return super(TreeCreate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(TreeCreate, self).get_context_data(**kwargs)
        context['icon'] = 'fa fa-tree'
        context['title'] = _('Ajout d\'un état des lieux d\'un arbre')
        context['js'] = js.DateInput
        return context


class TreeUpdate(PlaceDetailEditAuthMixin, UpdateView):
    model = Tree
    form_class = TreeForm
    template_name = 'normal_form.html'

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        try:
            return super(TreeUpdate, self).form_valid(form)
        except IntegrityError as e:
            # messages.error(self.request, e.__cause__)
            messages.error(self.request, _('Cette état des lieux existe déjà'))
            return HttpResponseRedirect(self.request.path)
        return super(TreeUpdate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(TreeUpdate, self).get_context_data(**kwargs)
        context['icon'] = 'mki mki-ruins'
        context['title'] = _('Modification d\'un état des lieux d\'un arbre')
        context['js'] = js.DateInput
        return context


class TreeDelete(PlaceDetailEditAuthMixin, DeleteView):
    model = Tree
    template_name = 'confirm_delete.html'

    def get_success_url(self):
        return reverse_lazy('sights:place_detail', kwargs={'pk': self.object.place_id})

    def get_context_data(self, **kwargs):
        context = super(TreeDelete, self).get_context_data(**kwargs)
        context['icon'] = 'fa fa-fw fa-trash'
        context['title'] = _('Suppression d\'un état des lieux d\'un arbre')
        context['message_alert'] = _('Êtes-vous certain de vouloir supprimer l\'état des lieux d\'un arbre')
        return context


class TreeDetail(PlaceDetailViewAuthMixin, DetailView):
    model = Tree
    template_name = 'sights/tree_detail.html'

    def get_context_data(self, **kwargs):
        context = super(TreeDetail, self).get_context_data(**kwargs)
        pk = self.kwargs.get('pk')
        # Rendu des sessions
        context['icon'] = 'fi-trees'
        context['title'] = _('Détail de l\'état des lieux d\'un arbre')
        context['js'] = """      
        """
        # Rendu des observations
        context['treegiteicon'] = 'fi-home'
        context['treegitetitle'] = _('Détails des gîtes')
        context['treegitecount'] = TreeGite.objects.filter(tree=pk).distinct().count()
        context['treegitetable'] = TreeGiteTable(TreeGite.objects.filter(tree=pk))
        return context


#################################################
#               TreeGite model views                #
#################################################


class TreeGiteCreate(LoginRequiredMixin, CreateView):
    model = TreeGite
    form_class = TreeGiteForm
    template_name = 'normal_form.html'

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        form.instance.tree_id = self.kwargs.get('pk')
        try:
            return super(TreeGiteCreate, self).form_valid(form)
        except IntegrityError as e:
            # messages.error(self.request, e.__cause__)
            messages.error(self.request, _('Ce gîte existe déjà'))
            return HttpResponseRedirect(self.request.path)
        return super(TreeGiteCreate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(TreeGiteCreate, self).get_context_data(**kwargs)
        context['icon'] = 'fa fa-tree'
        context['title'] = _('Ajout d\'un détail de gîte arboricole')
        context['js'] = """      
        """
        return context


class TreeGiteUpdate(TreeGiteEditAuthMixin, UpdateView):
    model = TreeGite
    form_class = TreeGiteForm
    template_name = 'normal_form.html'

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        form.instance.tree_id = self.kwargs.get('pk')
        try:
            return super(TreeGiteUpdate, self).form_valid(form)
        except IntegrityError as e:
            # messages.error(self.request, e.__cause__)
            messages.error(self.request, _('Ce gîte existe déjà'))
            return HttpResponseRedirect(self.request.path)
        return super(TreeGiteUpdate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(TreeGiteUpdate, self).get_context_data(**kwargs)
        context['icon'] = 'fa fa-tree'
        context['title'] = _('Modification d\'un détail de gîte arboricole')
        context['js'] = """      
        """
        return context


class TreeGiteDelete(TreeGiteEditAuthMixin, DeleteView):
    model = Tree
    template_name = 'confirm_delete.html'

    def get_success_url(self):
        return reverse_lazy('sights:place_detail', kwargs={'pk': self.object.place_id})

    def get_context_data(self, **kwargs):
        context = super(PlaceBuildDetailDelete, self).get_context_data(**kwargs)
        context['icon'] = 'fa fa-fw fa-trash'
        context['title'] = _('Suppression d\'un détail de gîte arboricole')
        context['message_alert'] = _('Êtes-vous certain de vouloir supprimer ce détail de gîte')
        return context


#################################################
#               Cave model views                #
#################################################


class CaveCreate(LoginRequiredMixin, CreateView):
    model = Cave
    form_class = CaveForm
    template_name = 'normal_form.html'

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        form.instance.place_id = self.kwargs.get('pk')
        try:
            return super(CaveCreate, self).form_valid(form)
        except IntegrityError as e:
            # messages.error(self.request, e.__cause__)
            messages.error(self.request, _('Cette état des lieux existe déjà'))
            return HttpResponseRedirect(self.request.path)
        return super(CaveCreate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CaveCreate, self).get_context_data(**kwargs)
        context['icon'] = 'mki mki-cave_entrance'
        context['title'] = _('Ajout d\'un état des lieux d\'une cavité')
        context['js'] = js.DateInput
        return context


class CaveUpdate(PlaceDetailEditAuthMixin, UpdateView):
    model = Cave
    form_class = CaveForm
    template_name = 'normal_form.html'

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        try:
            return super(CaveUpdate, self).form_valid(form)
        except IntegrityError as e:
            # messages.error(self.request, e.__cause__)
            messages.error(self.request, _('Cette état des lieux existe déjà'))
            return HttpResponseRedirect(self.request.path)
        return super(CaveUpdate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CaveUpdate, self).get_context_data(**kwargs)
        context['icon'] = 'mki mki-cave_entrance'
        context['title'] = _('Modification d\'un état des lieux d\'une cavité')
        context['js'] = js.DateInput
        return context


class CaveDelete(PlaceDetailEditAuthMixin, DeleteView):
    model = Cave
    template_name = 'confirm_delete.html'

    def get_success_url(self):
        return reverse_lazy('sights:place_detail', kwargs={'pk': self.object.place_id})

    def get_context_data(self, **kwargs):
        context = super(PlaceBuildDetailDelete, self).get_context_data(**kwargs)
        context['icon'] = 'fa fa-fw fa-trash'
        context['title'] = _('Suppression d\'un état des lieux d\'une cavité')
        context['message_alert'] = _('Êtes-vous certain de vouloir supprimer l\'état des lieux d\'une cavité')
        return context


#################################################
#               Bridge model views                #
#################################################


class BridgeCreate(LoginRequiredMixin, CreateView):
    model = Bridge
    form_class = BridgeForm
    template_name = 'normal_form.html'

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        form.instance.place_id = self.kwargs.get('pk')
        try:
            return super(BridgeCreate, self).form_valid(form)
        except IntegrityError as e:
            # messages.error(self.request, e.__cause__)
            messages.error(self.request, _('Cette état des lieux existe déjà'))
            return HttpResponseRedirect(self.request.path)
        return super(BridgeCreate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(BridgeCreate, self).get_context_data(**kwargs)
        context['icon'] = 'mki mki-cave_entrance'
        context['title'] = _('Ajout d\'un état des lieux d\'un pont')
        context['js'] = js.DateInput
        return context


class BridgeUpdate(PlaceDetailEditAuthMixin, UpdateView):
    model = Bridge
    form_class = BridgeForm
    template_name = 'normal_form.html'

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        try:
            return super(BridgeUpdate, self).form_valid(form)
        except IntegrityError as e:
            # messages.error(self.request, e.__cause__)
            messages.error(self.request, _('Cette état des lieux existe déjà'))
            return HttpResponseRedirect(self.request.path)
        return super(BridgeUpdate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(BridgeUpdate, self).get_context_data(**kwargs)
        context['icon'] = 'mki mki-cave_entrance'
        context['title'] = _('Modification d\'un état des lieux d\'un pont')
        context['js'] = js.DateInput
        return context


class BridgeDelete(PlaceDetailEditAuthMixin, DeleteView):
    model = Bridge
    template_name = 'confirm_delete.html'

    def get_success_url(self):
        return reverse_lazy('sights:place_detail', kwargs={'pk': self.object.place_id})

    def get_context_data(self, **kwargs):
        context = super(PlaceBuildDetailDelete, self).get_context_data(**kwargs)
        context['icon'] = 'fa fa-fw fa-trash'
        context['title'] = _('Suppression d\'un état des lieux d\'un pont')
        context['message_alert'] = _('Êtes-vous certain de vouloir supprimer l\'état des lieux d\'un pont')
        return context


#################################################
#             CountDetail model views           #
#################################################


class CountDetailCreate(LoginRequiredMixin, CreateView):
    model = CountDetail
    template_name = 'normal_form.html'

    def get_form_class(self):
        sighting_id = self.kwargs.get('pk')
        contact = Sighting.objects.get(id_sighting=sighting_id).session.contact.code
        if contact in ('vm', 'ca'):
            return CountDetailBiomForm
        if contact == 'du':
            return CountDetailAcousticForm
        if contact == 'te':
            return CountDetailTelemetryForm
        else:
            return CountDetailOtherForm

    def get_form_kwargs(self):
        kwargs = super(CountDetailCreate, self).get_form_kwargs()
        sighting_id = self.kwargs.get('pk')
        kwargs['sighting_id'] = sighting_id
        contact = Sighting.objects.get(id_sighting=sighting_id).session.contact.code
        kwargs['contact'] = contact
        if contact in ('vm', 'ca'):
            kwargs['session'] = Sighting.objects.get(id_sighting=sighting_id).session.id_session
        return kwargs

    def get_initial(self):
        initial = super(CountDetailCreate, self).get_initial()
        initial = initial.copy()
        sighting_id = self.kwargs.get('pk')
        contact = Sighting.objects.get(id_sighting=sighting_id).session.contact.code
        if contact in ('vm', 'ca'):
            initial['manipulator'] = self.request.user
            initial['validator'] = self.request.user
        if CountDetail.objects.filter(sighting_id=sighting_id):
            lastcountdetail = CountDetail.objects.filter(sighting_id=sighting_id).order_by('-timestamp_create')[0]
            if contact in ('vm', 'du'):
                if lastcountdetail:
                    initial['time'] = lastcountdetail.time
                    initial['method'] = lastcountdetail.method
                    initial['unit'] = lastcountdetail.unit
                    initial['precision'] = lastcountdetail.precision
                    initial['device'] = lastcountdetail.device
        return initial

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        form.instance.sighting_id = self.kwargs.get('pk')
        contact = Sighting.objects.get(id_sighting=self.kwargs.get('pk')).session.contact.code
        if contact == 'du':
            form.instance.precision = CountPrecision.objects.get(contact=contact)
        if contact in ('vm', 'te'):
            form.instance.count = 1
        return super(CountDetailCreate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CountDetailCreate, self).get_context_data(**kwargs)
        context['icon'] = 'fa fa-fw fa-venus-mars'
        context['title'] = _('Ajout d\'une observation détaillée')
        context['js'] = js.TimeInput
        return context

    def get_success_url(self):
        sighting_id = self.object.sighting_id
        if self.request.method == 'POST' and '_addanother' in self.request.POST:
            print('lala')
            return reverse_lazy('sights:countdetail_create', kwargs={'pk': sighting_id})
        return reverse_lazy('sights:sighting_detail', kwargs={'pk': self.object.sighting_id})


class CountDetailUpdate(CountDetailEditAuthMixin, UpdateView):
    model = CountDetail
    template_name = 'normal_form.html'

    def get_form_class(self):
        sighting_id = self.object.sighting_id
        contact = Sighting.objects.get(id_sighting=sighting_id).session.contact.code
        if contact in ('vm', 'ca'):
            return CountDetailBiomForm
        if contact == 'du':
            return CountDetailAcousticForm
        if contact == 'te':
            return CountDetailTelemetryForm
        else:
            return CountDetailOtherForm

    def get_form_kwargs(self):
        kwargs = super(CountDetailUpdate, self).get_form_kwargs()
        sighting_id = self.object.sighting_id
        kwargs['sighting_id'] = sighting_id
        contact = Sighting.objects.get(id_sighting=sighting_id).session.contact.code
        kwargs['contact'] = contact
        if contact in ('vm', 'ca'):
            kwargs['session'] = Sighting.objects.get(id_sighting=sighting_id).session.id_session
        return kwargs

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        contact = Sighting.objects.get(id_sighting=self.object.sighting_id).session.contact.code
        if contact == 'du':
            form.instance.precision = CountPrecision.objects.get(contact=contact)
        if contact in ('vm', 'te'):
            form.instance.count = 1
        return super(CountDetailUpdate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CountDetailUpdate, self).get_context_data(**kwargs)
        context['icon'] = 'fa fa-venus-mars'
        context['title'] = _('Modification d\'une observation détaillée')
        jsAddAnother = """   
        $('#addanother').hide();    
        """
        context['js'] = jsAddAnother + js.TimeInput
        return context


class CountDetailDelete(CountDetailEditAuthMixin, DeleteView):
    model = CountDetail
    template_name = 'confirm_delete.html'

    def get_success_url(self):
        return reverse_lazy('sights:sighting_detail', kwargs={'pk': self.object.sighting_id})

    def get_context_data(self, **kwargs):
        context = super(CountDetailDelete, self).get_context_data(**kwargs)
        context['icon'] = 'fa fa-fw fa-trash'
        context['title'] = _('Suppression d\'une observation détaillée')
        context['message_alert'] = _('Êtes-vous certain de vouloir supprimer l\'observation détaillée')
        return context
