import datetime

from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.gis.db import models as gismodels
from django.core.files.storage import FileSystemStorage
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db.models import Sum
from django.db.models.deletion import DO_NOTHING
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

from core.functions import get_sight_period
from dicts.models import *
from geodata.models import Territory, Municipality, LandCover
from management.models import Study, Transmitter

fs = FileSystemStorage()


class Place(models.Model):
    id_place = models.AutoField(primary_key=True, db_index=True)
    name = models.CharField(max_length=255, db_index=True,
                            verbose_name='nom du lieu')
    is_hidden = models.BooleanField(
        db_index=True, default=False, verbose_name='Site sensible')
    authorized_user = models.ManyToManyField(
        settings.AUTH_USER_MODEL, blank=True, related_name='hiddenplaceauthuser',
        verbose_name=_('Personnes authorisées si site sensible'))
    type = models.ForeignKey(
        TypePlace, db_index=True, blank=True, null=True, verbose_name=_("Type de gîte"))
    is_gite = models.BooleanField(
        db_index=True, verbose_name='Est un gîte', default=False)
    is_managed = models.BooleanField(db_index=True, default=False, verbose_name='Site géré')
    proprietary = models.CharField(
        max_length=100, db_index=True, blank=True, null=True, verbose_name='Nom du propriétaire')
    convention = models.BooleanField(
        verbose_name='Convention refuge', default=False)
    convention_file = models.FileField(
        upload_to='place/convention/', blank=True, null=True, verbose_name='Exemplaire de la convention')
    map_file = models.FileField(
        upload_to='place/map/', blank=True, null=True, verbose_name='Carte détaillée, topo, etc.')
    photo_file = models.ImageField(
        upload_to='photo/', blank=True, null=True, verbose_name='Photo de la localité')
    habitat = models.CharField(max_length=255, blank=True, null=True, verbose_name=_("Habitat"))
    altitude = models.PositiveIntegerField(blank=True, null=True,
                                           verbose_name=_("Altitude"),
                                           validators=[
                                               MaxValueValidator(settings.GEODATA_ALTI_MAX),
                                               MinValueValidator(settings.GEODATA_ALTI_MIN)
                                           ])
    domain = models.ForeignKey(
        PropertyDomain, max_length=100,
        on_delete=DO_NOTHING,
        null=True,
        blank=True, verbose_name=_("Domaine"))
    municipality = models.ForeignKey(
        Municipality, models.DO_NOTHING, db_index=True, blank=True, null=True, verbose_name=_("Commune"))
    territory = models.ForeignKey(
        Territory, models.SET_NULL, db_index=True, blank=True, null=True, verbose_name=_("Territoire"))
    landcover = models.ForeignKey(LandCover, db_index=True, blank=True, null=True,
                                  verbose_name=_("Occupation du sol"))
    id_bdcavite = models.CharField(max_length=15, blank=True, null=True)
    plan_localite = models.FileField(upload_to='place/plan_localite/', blank=True,
                                     null=True, verbose_name='Plan du site et/ou topo de la cavité')
    comment = models.TextField(blank=True,
                               null=True, verbose_name=_('Commentaire'))
    other_imported_data = models.TextField(blank=True,
                                           null=True, verbose_name=_('Autres données importées'))
    telemetric_crossaz = models.BooleanField(default=False, verbose_name=_('Croisement d \'azimuth télémétrique'))
    bdsource = models.CharField(max_length=100, blank=True, null=True)
    id_bdsource = models.CharField(max_length=100, blank=True, null=True)
    precision = models.ForeignKey(PlacePrecision, models.DO_NOTHING, verbose_name="Précision géographique")
    x = models.FloatField(
        blank=True, null=True, verbose_name='Longitude',
        help_text=_('Coordonnées est-ouest en WGS84 (projection habituelle des gps)'))
    y = models.FloatField(
        blank=True, null=True, verbose_name='Latitude',
        help_text=_('Coordonnées nord-sud en WGS84 (projection habituelle des gps)'))
    geom = gismodels.PointField(srid=settings.GEODATA_SRID, null=True, blank=True,
                                verbose_name='Localisation géographique')
    timestamp_create = models.DateTimeField(
        auto_now_add=True, editable=False)
    timestamp_update = models.DateTimeField(
        auto_now=True, editable=False)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=True, db_index=True, editable=False, related_name='place_creator')
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=True, db_index=True, editable=False, related_name='place_modifier')

    def __str__(self):
        return "%s ∙ %s" % (self.name, self.municipality)

    def get_absolute_url(self):
        return reverse('sights:place_detail', kwargs={'pk': self.id_place})

    def save(self, *args, **kwargs):
        self.y = self.geom.y
        self.x = self.geom.x
        try:
            self.territory = Territory.objects.get(geom__contains=self.geom)
        except:
            pass
        try:
            self.municipality = Municipality.objects.get(geom__contains=self.geom)
        except:
            pass
        # self.creator = current_user()
        super(Place, self).save(*args, **kwargs)

    class Meta:
        verbose_name = "Localité"
        verbose_name_plural = "Localités"
        indexes = [
            models.Index(fields=['-timestamp_update'],
                         name='place_timestamp_update_idx'),
            models.Index(fields=['name'], name='place_name_idx'),
            models.Index(fields=['created_by'], name='place_creator_idx'),
            # models.Index(fields=['municipality'],
            #             name='place_municipality_idx'),
            models.Index(fields=['territory'], name='place_territory_idx'),
            models.Index(fields=['geom'], name='place_geom_idx'),
        ]
        unique_together = ['name', 'municipality', 'type']


class PlaceManagement(models.Model):
    id_placemanagement = models.AutoField(primary_key=True)
    place = models.ForeignKey(Place, editable=False, related_name='management')
    date = models.DateField(verbose_name=_("Date"))
    action = models.ForeignKey(PlaceManagementAction, verbose_name=_("Action"))
    comment = models.TextField(blank=True, null=True, verbose_name=_("Commentaire"))
    referent = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_("Référent de l\'action"))
    file = models.FileField(upload_to='place/management/', blank=True, null=True, verbose_name=_("Fichier"),
                            help_text=_(
                                'Si vous voulez charger plusieurs fichiers, créez une archive avec un logiciel de compression comme 7zip'))
    bdsource = models.CharField(max_length=100, blank=True, null=True)
    id_bdsource = models.CharField(max_length=100, blank=True, null=True)
    timestamp_create = models.DateTimeField(
        auto_now_add=True, editable=False)
    timestamp_update = models.DateTimeField(
        auto_now=True, editable=False)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=True, db_index=True, editable=False, related_name='placemanagement_creator')
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=True, db_index=True, editable=False, related_name='placemanagement_modifier')

    def __str__(self):
        return "%s ∙ %s" % (self.date, self.action)

    def get_absolute_url(self):
        return reverse('sights:place_detail', kwargs={'pk': self.place_id})

    class Meta:
        verbose_name = _("Action de gestion")
        verbose_name_plural = _("Actions de gestion")


class Bridge(models.Model):
    id_bridge = models.AutoField(primary_key=True)
    place = models.ForeignKey(Place)
    visit_date = models.DateField()
    interest = models.ForeignKey(
        Interest, null=True, blank=True, verbose_name='Intérêt pour les chiros')
    renovated = models.NullBooleanField(verbose_name='Rénové')
    renovated_date = models.DateField(
        null=True, blank=True, verbose_name='Date de rénovation')
    joint = models.NullBooleanField(verbose_name='Joints')
    rift = models.NullBooleanField(verbose_name='Fissures')
    expansion = models.NullBooleanField(verbose_name='Joints de dilatations')
    drain = models.NullBooleanField(verbose_name='Drains')
    cornice = models.NullBooleanField(verbose_name='Corniche')
    volume = models.NullBooleanField(verbose_name='Volume')
    comment = models.TextField(blank=True,
                               null=True, verbose_name=_('Commentaire'))
    bdsource = models.CharField(max_length=100, blank=True, null=True)
    id_bdsource = models.CharField(max_length=100, blank=True, null=True)
    timestamp_create = models.DateTimeField(
        auto_now_add=True, editable=False)
    timestamp_update = models.DateTimeField(
        auto_now=True, editable=False)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=True, db_index=True, editable=False, related_name='bridge_creator')
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=True, db_index=True, editable=False, related_name='bridge_modifier')

    def __str__(self):
        return "Pont : %s ∙ %s" % (self.place.name, self.visit_date)

    def get_absolute_url(self):
        return reverse('sights:place_detail', kwargs={'pk': self.place_id})

    class Meta:
        verbose_name = "Détails d'un pont"
        verbose_name_plural = "Détails des ponts"
        unique_together = ["visit_date", "place"]


class Build(models.Model):
    id_build = models.AutoField(primary_key=True)
    place = models.ForeignKey(Place, on_delete=models.CASCADE)
    visit_date = models.DateField(verbose_name='Date de visite')
    cavity_front = models.NullBooleanField(verbose_name='Cavité en façade')
    attic = models.NullBooleanField(verbose_name='Combles présents')
    attic_access = models.ForeignKey(
        GiteBatAccess,
        models.DO_NOTHING,
        blank=True,
        null=True,
        verbose_name='Accessibilité aux chiroptères',
        related_name='attic')
    bell_tower = models.NullBooleanField(verbose_name='Clocher présent')
    bell_tower_screen = models.NullBooleanField(
        verbose_name='Clocher grillagé')
    bell_tower_access = models.ForeignKey(
        GiteBatAccess,
        models.DO_NOTHING,
        blank=True,
        null=True,
        verbose_name='Accessibilité aux chiroptères',
        related_name='belltower')
    cover = models.ForeignKey(BuildCover,
                              models.DO_NOTHING,
                              blank=True,
                              null=True,
                              verbose_name='Type de couverture')
    cellar = models.NullBooleanField(verbose_name='Cave présente')
    cellar_access = models.ForeignKey(
        GiteBatAccess,
        models.DO_NOTHING,
        blank=True,
        null=True,
        verbose_name='Accessibilité aux chiroptères',
        related_name='cellar')
    ext_light = models.NullBooleanField(verbose_name='Eclairage extérieur')
    access_light = models.NullBooleanField(verbose_name='Entrées éclairées')
    comment = models.TextField(blank=True,
                               null=True, verbose_name=_('Commentaire'))
    bdsource = models.CharField(max_length=100, blank=True, null=True)
    id_bdsource = models.CharField(max_length=100, blank=True, null=True)
    timestamp_create = models.DateTimeField(
        auto_now_add=True, editable=False)
    timestamp_update = models.DateTimeField(
        auto_now=True, editable=False)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=True, db_index=True, editable=False, related_name='build_creator')
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=True, db_index=True, editable=False, related_name='build_modifier')

    def __str__(self):
        return "Pont : %s ∙ %s" % (self.place.name, self.visit_date)

    def get_absolute_url(self):
        return reverse('sights:place_detail', kwargs={'pk': self.place_id})

    class Meta:
        verbose_name = "Détails d'un pont"
        verbose_name_plural = "Détails des ponts"
        unique_together = ["visit_date", "place"]


class Tree(models.Model):
    """

    # SubModel of place model for tree bat gites.

    """
    id_tree = models.AutoField(primary_key=True)
    place = models.ForeignKey(Place, on_delete=models.CASCADE)
    visit_date = models.DateField(verbose_name='Date', help_text='Date de visite')
    context = models.ForeignKey(TreeContext, on_delete=models.SET_NULL, null=True, blank=True,
                                verbose_name=_('Contexte'))
    forest_stands = models.ForeignKey(TreeForestStands, on_delete=models.SET_NULL, blank=True, null=True,
                                      verbose_name=_('Peuplement'), help_text=_('Peuplement forestier'))
    situation = models.CharField(max_length=100, blank=True, null=True,
                                 verbose_name=_('Situation'),
                                 help_text=_('... dans le milieu (dans le bois, en lisière, etc.)'))
    circumstance = models.ForeignKey(TreeCircumstance, on_delete=models.SET_NULL, null=True, blank=True,
                                     verbose_name=_('Circumstance'), help_text=_('Circumstance de la découverte'))
    tree_specie = models.ForeignKey(
        TreeSpecies, null=True, blank=True, verbose_name=_('Espèce'), help_text=_('Espèce de l\'arbre'))
    health = models.ForeignKey(TreeHealth, null=True, blank=True, verbose_name=_('Etat'), help_text=_('Etat sanitaire'))
    tree_diameter = models.PositiveIntegerField(null=True, blank=True,
                                                verbose_name=_('Diamètre'),
                                                help_text=_('à 1.30m du sol, en cm'),
                                                validators=[
                                                    MaxValueValidator(500),
                                                ])
    standing = models.NullBooleanField(verbose_name=_('Arbre debout'))
    protected = models.NullBooleanField(verbose_name=_('Arbre protégé'))
    bat_specie = models.ManyToManyField(
        Specie, blank=True, related_name='treebatspecie',
        verbose_name=_('Espèces observées'))
    gite_type = models.ManyToManyField(TreeGiteType, blank=True, verbose_name=_('Types de gîte(s)'))
    gite_origin = models.ManyToManyField(TreeGiteOrigin, blank=True, verbose_name=_('Origines du/des gîte(s)'))
    gite_localisation = models.ManyToManyField(TreeGitePlace, blank=True,
                                               verbose_name=_('Emplacement du/des gîte(s)'))
    comment = models.TextField(blank=True, null=True, verbose_name=_('Commentaire'))
    bdsource = models.CharField(max_length=100, blank=True, null=True)
    id_bdsource = models.CharField(max_length=100, blank=True, null=True)
    timestamp_create = models.DateTimeField(
        auto_now_add=True, editable=False)
    timestamp_update = models.DateTimeField(
        auto_now=True, editable=False)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=True, db_index=True, editable=False, related_name='tree_creator')
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=True, db_index=True, editable=False, related_name='tree_modifier')

    def __str__(self):
        return "Arbre : %s ∙ %s" % (self.place.name, self.visit_date)

    def get_absolute_url(self):
        return reverse('sights:tree_detail', kwargs={'pk': self.id_tree})

    class Meta:
        verbose_name = "Détail d'un arbre"
        verbose_name_plural = "Détails des arbres"
        unique_together = ["visit_date", "place"]


class TreeGite(models.Model):
    id_treegite = models.AutoField(primary_key=True)
    tree = models.ForeignKey(Tree, on_delete=models.CASCADE)
    bat_specie = models.ManyToManyField(
        Specie, blank=True, related_name='treegitebatspecie',
        verbose_name=_('Espèces observées'))
    gite_type = models.ForeignKey(TreeGiteType, null=True, blank=True, verbose_name=_('Type de gîte'))
    gite_origin = models.ForeignKey(TreeGiteOrigin, null=True, blank=True, verbose_name=_('Origine du gîte'))
    gite_localisation = models.ForeignKey(TreeGitePlace, null=True, blank=True,
                                          verbose_name=_('Emplacement du gîte'))
    gite_high = models.PositiveIntegerField(
        null=True, blank=True, verbose_name=_('Hauteur gîte (au plus haut point d’accès)'))
    gite_tree_diameter = models.DecimalField(max_digits=3, decimal_places=1,
                                             null=True, blank=True,
                                             verbose_name=_('Diamètre (branche, tronc) à hauteur du gîte'),
                                             help_text=_('en cm'))
    gite_access_orientation = models.CharField(max_length=50, blank=True, null=True,
                                               verbose_name=_('Orientation du ou des accès'))
    gite_access_size = models.FloatField(blank=True, null=True, verbose_name=_(
        'largeur ou diamètre de l\'ouverture en cm'))
    comment = models.TextField(blank=True,
                               null=True, verbose_name=_('Commentaire'))
    bdsource = models.CharField(max_length=100, blank=True, null=True)
    id_bdsource = models.CharField(max_length=100, blank=True, null=True)
    timestamp_create = models.DateTimeField(
        auto_now_add=True, editable=False)
    timestamp_update = models.DateTimeField(
        auto_now=True, editable=False)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=True, db_index=True, editable=False, related_name='treegite_creator')
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=True, db_index=True, editable=False, related_name='treegite_modifier')

    def get_absolute_url(self):
        return reverse('sights:tree_detail', kwargs={'pk': self.tree_id})

    class Meta:
        verbose_name = "Détail des gîtes d'un arbre"
        verbose_name_plural = "Détails des gîtes d'un arbre"


class Cave(models.Model):
    id_cave = models.AutoField(primary_key=True)
    place = models.ForeignKey(Place, on_delete=models.CASCADE)
    bd_cavite = models.CharField(
        max_length=20, blank=True, null=True, verbose_name='Identifiant de la BD Cavités du BRGM')
    visit_date = models.DateField()
    interest = models.ForeignKey(
        Interest, null=True, blank=True, verbose_name='Intérêt pour les chiros')
    length = models.PositiveIntegerField(
        blank=True, null=True, verbose_name='Développement')
    altdiff = models.IntegerField(
        blank=True, null=True, verbose_name='Dénivellé vers la cavité')
    n_entry = models.PositiveIntegerField(
        blank=True, null=True, verbose_name='Nombre d''entrées')
    equip_req = models.NullBooleanField(
        verbose_name='Equipement requis?')
    equipment = models.TextField(blank=True, null=True, verbose_name=_("Equipement nécessaire"))
    access_walk_duration = models.TimeField(
        blank=True, null=True, verbose_name=_("Durée de la marche d'accès"))
    comment = models.TextField(blank=True,
                               null=True, verbose_name=_('Commentaire'))
    bdsource = models.CharField(max_length=100, blank=True, null=True)
    id_bdsource = models.CharField(max_length=100, blank=True, null=True)
    timestamp_create = models.DateTimeField(
        auto_now_add=True, editable=False)
    timestamp_update = models.DateTimeField(
        auto_now=True, editable=False)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=True, db_index=True, editable=False, related_name='cave_creator')
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=True, db_index=True, editable=False, related_name='cave_modifier')

    def __str__(self):
        return _("Cavité : %s ∙ %s") % (self.place.name, self.visit_date)

    def get_absolute_url(self):
        return reverse('sights:place_detail', kwargs={'pk': self.place_id})

    class Meta:
        verbose_name = "Détail d'une cavité"
        verbose_name_plural = "Détails des cavités"
        unique_together = ["visit_date", "place"]


class Session(models.Model):
    id_session = models.AutoField(primary_key=True)
    name = models.CharField(
        max_length=150, blank=True, verbose_name='Intitulé de la session')
    contact = models.ForeignKey(
        Contact,
        models.DO_NOTHING, null=True, verbose_name=_("Type de contact"))
    place = models.ForeignKey(
        Place,
        on_delete=models.CASCADE,
        verbose_name='Localité associée')
    date_start = models.DateField(
        verbose_name='Date de début', help_text=_('Format de date: <em>01/01/2017</em>.'))
    time_start = models.TimeField(
        blank=True, null=True, verbose_name=_('Heure de début'), help_text=_('Format d\'heure: <em>02:15</em>.'))
    date_end = models.DateField(
        blank=True, null=True, verbose_name=_('Date de fin'), help_text=_('Format de date: <em>01/01/2017</em>.'))
    time_end = models.TimeField(
        blank=True, null=True, verbose_name=_('Heure de fin'), help_text=_('Format d\'heure: <em>02:15</em>.'))
    data_file = models.FileField(
        upload_to='session/file/', blank=True, null=True,
        verbose_name='Fichier de données (tableur d\'analyse acoustique, fiche de terrain, etc.)')
    main_observer = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=DO_NOTHING, related_name='mainobs')
    other_observer = models.ManyToManyField(
        settings.AUTH_USER_MODEL, blank=True, related_name='otherobs')
    is_confidential = models.BooleanField(default=False, verbose_name=_('Données confidentielles'))
    study = models.ForeignKey(
        Study,
        models.DO_NOTHING,
        verbose_name='Etude',
        related_name='study',
        blank=True,
        null=True)
    comment = models.TextField(blank=True,
                               null=True, verbose_name=_('Commentaire'))
    bdsource = models.CharField(max_length=100, blank=True, null=True)
    id_bdsource = models.TextField(blank=True, null=True)
    timestamp_create = models.DateTimeField(
        auto_now_add=True, editable=False)
    timestamp_update = models.DateTimeField(
        auto_now=True, editable=False)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=True, db_index=True, editable=False, related_name='session_creator')
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=True, db_index=True, editable=False, related_name='session_modifier')

    def __str__(self):
        return "%s ∙ %s ∙ %s" % (
            self.place, datetime.date(self.date_start.year, self.date_start.month, self.date_start.day), self.contact)

    def save(self, *args, **kwargs):
        self.name = "loc%s %s %s %s" % (self.place_id, datetime.date(
            self.date_start.year, self.date_start.month, self.date_start.day), self.contact.code,
                                        self.main_observer.username)
        super(Session, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('sights:session_detail', kwargs={'pk': self.id_session})

    class Meta:
        verbose_name = "Session d'inventaire/observations"
        verbose_name_plural = "Sessions d'inventaire/observations"
        unique_together = ["place", "contact", "date_start"]


class Sighting(models.Model):
    id_sighting = models.AutoField(_('id unique'), primary_key=True)
    session = models.ForeignKey(
        Session, on_delete=models.CASCADE, verbose_name=_('Session associée'))
    period = models.CharField(max_length=50, blank=True, null=True, verbose_name=_('Période d\'observation'))
    codesp = models.ForeignKey(
        Specie, on_delete=DO_NOTHING, verbose_name=_('Espèce ou groupe d\'espèce'))
    total_count = models.PositiveIntegerField('Nombre total', blank=True, null=True, help_text=_('Désactivé pour les données acoustiques et en main'))
    breed_colo = models.NullBooleanField(
        verbose_name='Colonie de reproduction')
    observer = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name="sightingobs", verbose_name=_('Observateur'))
    is_doubtful = models.BooleanField(default=False, verbose_name=_('Donnée douteuse'))
    id_bdsource = models.TextField(
        'id unique de la bdd source', blank=True, null=True)
    bdsource = models.CharField(
        'bdd source', max_length=100, blank=True, null=True)
    comment = models.TextField(blank=True,
                               null=True, verbose_name=_('Commentaire'))
    timestamp_create = models.DateTimeField(
        auto_now_add=True, editable=False)
    timestamp_update = models.DateTimeField(
        auto_now=True, editable=False)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=True, db_index=True, editable=False, related_name='sighting_creator')
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=True, db_index=True, editable=False, related_name='sighting_modifier')

    def __str__(self):
        return "%s ∙ %s ∙ %s" % (self.session, self.codesp, self.total_count)

    def get_absolute_url(self):
        return reverse('sights:sighting_detail', kwargs={'pk': self.id_sighting})

    def save(self, *args, **kwargs):
        self.period = get_sight_period(self.session.date_start)
        # self.creator = current_user()
        super(Sighting, self).save(*args, **kwargs)

    class Meta:
        verbose_name = "Observation"
        verbose_name_plural = "Observations"
        unique_together = ["codesp", "session"]


class Device(models.Model):
    id_device = models.AutoField(primary_key=True)
    session = models.ForeignKey(
        Session, on_delete=models.CASCADE, verbose_name='Session associée')
    ref = models.CharField(max_length=30, verbose_name=_('Référence du dispositif pour la session'), default='nd')
    type = models.ForeignKey(TypeDevice, on_delete=models.CASCADE)
    height = models.FloatField(blank=True, null=True, default=None, verbose_name=_('Hauteur'))
    width = models.FloatField(blank=True, null=True, default=None, verbose_name=_('Largeur'))
    context = models.CharField(max_length=150, blank=True, null=True, verbose_name=_('Contexte'), default=None)
    photo_file = models.ImageField(
        upload_to='photo/', blank=True, null=True, verbose_name=_('Photo du dispositif'), default=None)
    # geom = models.GeometryCollectionField(srid=2154, null=True, blank=True,
    #                                       verbose_name=_('Localisation du dispositif'))
    id_fromsrc = models.CharField(
        'id unique de la bdd source', max_length=100, blank=True, null=True)
    bdsource = models.CharField(
        'bdd source', max_length=100, blank=True, null=True)
    comment = models.TextField(blank=True,
                               null=True, verbose_name=_('Commentaire'))
    timestamp_create = models.DateTimeField(
        auto_now_add=True, editable=False)
    timestamp_update = models.DateTimeField(
        auto_now=True, editable=False)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=True, db_index=True, editable=False, related_name='device_creator')
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=True, db_index=True, editable=False, related_name='device_modifier')

    def __str__(self):
        return self.ref

    def get_absolute_url(self):
        return reverse('sights:session_detail', kwargs={'pk': self.session.id_session})


class CountDetail(models.Model):
    id_countdetail = models.AutoField(primary_key=True)
    sighting = models.ForeignKey(Sighting,
                                 on_delete=models.CASCADE, related_name='countdetail_sighting')
    method = models.ForeignKey(Method, on_delete=models.DO_NOTHING, verbose_name=_('Méthode'), blank=True,
                               null=True)
    sex = models.ForeignKey(
        Sex,
        models.DO_NOTHING,
        blank=True,
        null=True,
        verbose_name=_('Sexe'))
    age = models.ForeignKey(
        Age,
        models.DO_NOTHING,
        blank=True,
        null=True,
        verbose_name=_('Age estimé'))
    precision = models.ForeignKey(
        CountPrecision, on_delete=DO_NOTHING, blank=True, null=True, verbose_name=_('Précision du comptage'))
    count = models.PositiveIntegerField(blank=True, null=True, verbose_name=_('Effectif'))
    unit = models.ForeignKey(
        CountUnit, on_delete=DO_NOTHING, blank=True, null=True, verbose_name=_('Unité'))
    time = models.TimeField(blank=True, null=True, verbose_name=_('Heure'), help_text=_('au format hh:mm'))
    device = models.ForeignKey(Device, on_delete=DO_NOTHING, blank=True, null=True, default=None,
                               verbose_name=_('Dispositif'))
    manipulator = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name="observer", blank=True, null=True, verbose_name=_('Manipulateur'))
    validator = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name="validator", blank=True, null=True, verbose_name=_('Validateur'))
    transmitter = models.ForeignKey(
        Transmitter, models.DO_NOTHING, blank=True, null=True, verbose_name=_('Emetteur de télémétrie'))
    ab = models.FloatField(blank=True, null=True, verbose_name=_('AB'), help_text=_('en mm'))
    d5 = models.FloatField(blank=True, null=True, verbose_name=_('D5'), help_text=_('en mm'))
    d3 = models.FloatField(blank=True, null=True, verbose_name=_('D3'), help_text=_('en mm'))
    pouce = models.FloatField(blank=True, null=True, verbose_name=_('Pouce'), help_text=_('en mm'))
    queue = models.FloatField(blank=True, null=True, verbose_name=_('Queue'), help_text=_('en mm'))
    tibia = models.FloatField(blank=True, null=True, verbose_name=_('Tibia'), help_text=_('en mm'))
    pied = models.FloatField(blank=True, null=True, verbose_name=_('Pied'), help_text=_('en mm'))
    cm3 = models.FloatField(blank=True, null=True, verbose_name=_('CM3'), help_text=_('en mm'))
    tragus = models.FloatField(blank=True, null=True, verbose_name=_('Tragus'), help_text=_('en mm'))
    poids = models.FloatField(blank=True, null=True, verbose_name=_('Poids'), help_text=_('en g'))
    testicule = models.ForeignKey(
        BiomTesticule,
        models.DO_NOTHING,
        blank=True,
        null=True,
        verbose_name='Testicules')
    epididyme = models.ForeignKey(
        BiomEpipidyme,
        models.DO_NOTHING,
        blank=True,
        null=True,
        verbose_name='Epididymes')
    tuniq_vag = models.ForeignKey(
        BiomTuniqVag,
        models.DO_NOTHING,
        blank=True,
        null=True,
        verbose_name='Tuniques vaginales')
    gland_taille = models.ForeignKey(
        BiomGlandTaille,
        models.DO_NOTHING,
        blank=True,
        null=True,
        verbose_name='Taille des glandes')
    gland_coul = models.ForeignKey(
        BiomGlandCoul,
        models.DO_NOTHING,
        blank=True,
        null=True,
        verbose_name='couleur des glandes')
    mamelle = models.ForeignKey(
        BiomMamelle,
        models.DO_NOTHING,
        blank=True,
        null=True,
        verbose_name='Mamelles')
    gestation = models.ForeignKey(
        BiomGestation,
        models.DO_NOTHING,
        blank=True,
        null=True,
        verbose_name='Gestation')
    epiphyse = models.ForeignKey(
        BiomEpiphyse,
        models.DO_NOTHING,
        blank=True,
        null=True,
        verbose_name='Epiphyse articulaire')
    chinspot = models.ForeignKey(
        BiomChinspot,
        models.DO_NOTHING,
        blank=True,
        null=True,
        verbose_name='Tâche mentonière')
    usure_dent = models.ForeignKey(
        BiomDent,
        models.DO_NOTHING,
        blank=True,
        null=True,
        verbose_name='Usure des dents')
    etat_sexuel = models.CharField(max_length=2, blank=True,
                                   null=True, editable=False,
                                   verbose_name='Etat sexuel')
    comment = models.TextField(blank=True,
                               null=True, verbose_name=_('Commentaire'))
    id_fromsrc = models.CharField(
        'id unique de la bdd source', max_length=100, blank=True, null=True)
    bdsource = models.CharField(
        'bdd source', max_length=100, blank=True, null=True)
    other_imported_data = models.TextField(blank=True,
                                           null=True, verbose_name=_('Autres données importées'))
    timestamp_create = models.DateTimeField(
        auto_now_add=True, editable=False)
    timestamp_update = models.DateTimeField(
        auto_now=True, editable=False)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=True, db_index=True, editable=False, related_name='countdetail_creator')
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=True, db_index=True, editable=False, related_name='countdetail_modifier')

    def __str__(self):
        return "%s ∙ %s ∙ %s ∙ %s" % (self.sighting.session.place.name, self.sex, self.age, self.count)

    def get_absolute_url(self):
        return reverse('sights:sighting_detail', kwargs={'pk': self.sighting_id})

    class Meta:
        verbose_name = "4 ∙ Comptage détaillé"
        verbose_name_plural = "4 ∙ Comptages détaillés"


@receiver([post_save, post_delete], sender=CountDetail)
def sighting_total_count(sender, instance, **kwargs):
    sighting_id = instance.sighting_id
    contact = Sighting.objects.get(id_sighting=sighting_id).session.contact.code
    sighting = Sighting.objects.get(id_sighting=sighting_id)
    if contact != 'du':
        sighting.total_count = CountDetail.objects.filter(sighting_id=sighting_id).aggregate(Sum('count'))[
        'count__sum']
    sighting.save()
