import datetime

from django.template.defaultfilters import date as trdate
from django.utils.translation import ugettext_lazy as _
from django.views.generic import TemplateView

from dicts.models import Contact
from geodata.models import Territory
from sights.models import Sighting, Session, Place
from accounts.models import Profile

import arrow


class GlobalContribView(TemplateView):
    template_name = 'synthesis/contrib.html'

    def get_context_data(self, **kwargs):
        context = super(GlobalContribView, self).get_context_data(**kwargs)
        context['icon'] = 'fi-graph-trend'
        context['title'] = _('En synthèse')
        context['month_contrib_title'] = _('Observations au cours des 12 derniers mois')
        context['month_contrib'] = self.month_contrib()
        context['month_sights'] = self.month_sights()
        context['list_month'] = self.list_month()
        context['list_year'] = self.list_year()
        context['year_contrib_title'] = _('Nombre total de données par année')
        context['year_contrib'] = self.year_contrib()
        context['sight_date_label'] = _('par date d\'observation')
        context['create_date_label'] = _('par date de saisie')
        context['year_sights'] = self.year_sights()
        context['year'] = arrow.now().year
        context['contact_contrib_title'] = _('Nombre total de données par type de contact')
        context['list_contact'] = self.list_contact()
        context['contact_contrib'] = self.contact_contrib()
        context['territory_contrib_title'] = _('Observations totales par départements')
        context['list_territory'] = self.list_territory()
        context['territory_contrib'] = self.territory_contrib()
        context['sightingsicon'] = 'fi-eye'
        context['sightingscount'] = Sighting.objects.all().count()
        context['sightingstext'] = _('observations')
        context['placesicon'] = 'fi-marker'
        context['placescount'] = Place.objects.all().count()
        context['placestext'] = _('localités')
        context['sessionsicon'] = 'fi-calendar'
        context['sessionscount'] = Session.objects.all().count()
        context['sessionstext'] = _('sessions')
        context['observersicon'] = 'fi-torsos-all-female'
        context['observerscount'] = Profile.objects.all().count()
        context['observerstext'] = _('observateurs')
        return context

    def month_sights(self):
        final_data = []
        date = arrow.now()
        for month in range(12):
            date = date.replace(months=-1)
            count = Sighting.objects.filter(
                session__date_start__gte=date.floor('month').datetime,
                session__date_start__lte=date.ceil('month').datetime).count()
            final_data.append(count)
        return final_data[::-1]

    def month_contrib(self):
        final_data = []
        date = arrow.now()
        for month in range(12):
            date = date.replace(months=-1)
            count = Sighting.objects.filter(
                timestamp_create__gte=date.floor('month').datetime,
                timestamp_create__lte=date.ceil('month').datetime).count()
            final_data.append(count)
        return final_data[::-1]

    def list_month(self):
        final_data = []
        date = arrow.now()
        for month in range(12):
            date = date.replace(months=-1)
            datetime = date.floor('month').datetime
            months = trdate(date, 'M y')
            final_data.append(months)
        return final_data[::-1]

    def list_year(self):
        list_year = []
        last_year = arrow.now().year
        first_year = last_year - 10
        for year in range(first_year, last_year):
            year = year+1
            list_year.append(year)
        return list_year

    def year_sights(self):
        final_data = []
        for year in self.list_year():
            date = arrow.get(year,1,1)
            # date = date.replace(years=-1)
            count = Sighting.objects.filter(
                session__date_start__gte=date.floor('year').datetime,
                session__date_start__lte=date.ceil('year').datetime).count()
            final_data.append(count)
        return final_data

    def year_contrib(self):
        final_data = []
        for year in self.list_year():
            date = arrow.get(year,1,1)
            # date = date.replace(years=-1)
            count = Sighting.objects.filter(
                timestamp_create__gte=date.floor('year').datetime,
                timestamp_create__lte=date.ceil('year').datetime).count()
            final_data.append(count)
        return final_data

    def list_contact(self):
        final_data = []
        for contact in Contact.objects.values_list('code', flat=True).distinct():
            final_data.append(contact)
        return final_data

    def contact_contrib(self):
        final_data = []
        for contact in Contact.objects.values_list('code', flat=True).distinct():
            count = Sighting.objects.filter(session__contact__code=contact).count()
            final_data.append(count)
        return final_data

    def list_territory(self):
        final_data = []
        territories = Territory.objects.filter(coverage=True).values_list('name', flat=True).distinct().order_by(
            'code')
        for territory in territories:
            final_data.append(territory)
        return final_data

    def territory_contrib(self):
        final_data = []
        territories = self.list_territory()
        for territory in territories:
            count = Sighting.objects.filter(
                session__place__territory__name=territory).count()
            final_data.append(count)
        return final_data
