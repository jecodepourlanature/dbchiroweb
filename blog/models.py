from ckeditor.fields import RichTextField
from django.conf import settings
from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _


# Create your models here.


class Actu(models.Model):
    id_actu = models.AutoField(primary_key=True)
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL, editable=True, verbose_name=_('Auteur'))
    title = models.CharField(max_length=200, verbose_name=_('Titre'))
    briefdescr = models.TextField(max_length=2048, verbose_name=_('Chapeau'))
    text = RichTextField(verbose_name=_('Corps de l\'article'))
    timestamp_create = models.DateTimeField(
        auto_now_add=True, editable=False)
    timestamp_update = models.DateTimeField(
        auto_now=True, editable=False)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=True, db_index=True, editable=False, related_name='actu_creator')
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=True, db_index=True, editable=False, related_name='actu_modifier')

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('blog:actu_detail', kwargs={'pk': self.id_actu})

    class Meta:
        verbose_name = "Actualité du site"
        verbose_name_plural = "Actualités du site"
