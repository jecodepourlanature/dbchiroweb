"""
	Mixins
"""

from django.conf import settings
from django.contrib.auth import get_user_model
from django.shortcuts import redirect


class BlogCreateAuthMixin:
    '''
    Classe mixin de vérification que l'utilisateur possède les droits de créer le compte
    '''

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
        else:
            loggeduser = get_user_model().objects.get(id=request.user.id)
            if loggeduser.edit_all_data or loggeduser.access_all_data or loggeduser.is_resp or loggeduser.is_staff or loggeduser.is_superuser :
                return super(BlogCreateAuthMixin, self).dispatch(request, *args, **kwargs)
            else:
                return redirect('core:view_unauth')

class BlogModifyAuthMixin:
    '''
    Classe mixin de vérification que l'utilisateur possède les droits de créer le compte
    '''

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
        else:
            loggeduser = get_user_model().objects.get(id=request.user.id)
            obj = self.get_object()
            if loggeduser.edit_all_data or loggeduser.access_all_data or loggeduser.is_resp or loggeduser.is_staff or loggeduser.is_superuser or loggeduser == obj.created_by:
                return super(BlogModifyAuthMixin, self).dispatch(request, *args, **kwargs)
            else:
                return redirect('core:view_unauth')
