from django.contrib import admin
from .models import Actu

class ActuAdmin(admin.ModelAdmin):
    list_display = ('title', 'author', 'timestamp_create', 'timestamp_update')
    fieldsets = [
        (None, {'fields': ('title', 'briefdescr', 'text')}),
    ]

    def save_model(self, request, obj, form, change):
        if getattr(obj, 'author', None) is None:
            obj.author = request.user
        obj.save()

# Register your models here.
admin.site.register(Actu, ActuAdmin)