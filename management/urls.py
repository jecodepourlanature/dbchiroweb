from django.conf.urls import url

from .views import StudyList, StudyCreate, StudyUpdate, StudyDelete, TransmitterCreate, TransmitterList, \
    TransmitterUpdate, TransmitterDelete, CatchAuthCreate, CatchAuthUpdate, CatchAuthDelete, CatchAuthList

urlpatterns = [
    # Place relative URLS
    url(r'^study/add$',
        StudyCreate.as_view(), name='study_create'),
    url(r'^study/(?P<pk>[0-9]+)/update$',
        StudyUpdate.as_view(), name='study_update'),
    url(r'^study/(?P<pk>[0-9]+)/delete$',
        StudyDelete.as_view(), name='study_delete'),
    url(r'^study/list$',
        StudyList.as_view(), name='study_list'),
    url(r'^study/list/(?P<page>\d+)$',
        StudyList.as_view(), name='url_study_list'),
    # Transmitter relative URLS
    url(r'^transmitter/add$',
        TransmitterCreate.as_view(), name='transmitter_create'),
    url(r'^transmitter/(?P<pk>[0-9]+)/update$',
        TransmitterUpdate.as_view(), name='transmitter_update'),
    url(r'^transmitter/(?P<pk>[0-9]+)/delete$',
        TransmitterDelete.as_view(), name='transmitter_delete'),
    url(r'^transmitter/list$',
        TransmitterList.as_view(), name='transmitter_list'),
    url(r'^transmitter/list/(?P<page>\d+)$',
        TransmitterList.as_view(), name='url_transmitter_list'),
    # CatchAuth relative URLS
    url(r'^catchauth/add$',
        CatchAuthCreate.as_view(), name='catchauth_create'),
    url(r'^catchauth/(?P<pk>[0-9]+)/update$',
        CatchAuthUpdate.as_view(), name='catchauth_update'),
    url(r'^catchauth/(?P<pk>[0-9]+)/delete$',
        CatchAuthDelete.as_view(), name='catchauth_delete'),
    url(r'^catchauth/list$',
        CatchAuthList.as_view(), name='catchauth_list'),
    url(r'^catchauth/list/(?P<page>\d+)$',
        CatchAuthList.as_view(), name='url_catchauth_list'),
]
