"""
	Vues de l'application Sights
"""

from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.files.storage import FileSystemStorage
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django_tables2 import SingleTableView

from .forms import StudyForm, TransmitterForm, CatchAuthForm
from .mixins import ManagementAuthMixin, CatchAuthCreateAuthMixin
from .models import Study, Transmitter, CatchAuth
from .tables import StudyTable, TransmitterTable, CatchAuthTable
from core import js


class StudyCreate(LoginRequiredMixin, CreateView):
    """Create view for the Study model."""

    model = Study
    form_class = StudyForm
    template_name = 'normal_form.html'

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(StudyCreate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(StudyCreate, self).get_context_data(**kwargs)
        context['icon'] = 'fi-page'
        context['title'] = _('Ajout d\'une étude')
        context['js'] = """     
        """
        return context


class StudyUpdate(ManagementAuthMixin, UpdateView):
    model = Study
    form_class = StudyForm
    template_name = 'normal_form.html'

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(StudyUpdate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(StudyUpdate, self).get_context_data(**kwargs)
        context['icon'] = 'fi-page'
        context['title'] = _('Ajout d\'une étude')
        context['js'] = """      
        """
        return context


class StudyDelete(ManagementAuthMixin, DeleteView):
    model = Study
    template_name = 'confirm_delete.html'
    success_url = reverse_lazy('management:study_list')

    def get_context_data(self, **kwargs):
        context = super(StudyDelete, self).get_context_data(**kwargs)
        context['icon'] = 'fi-trash'
        context['title'] = _('Suppression d\'une étude')
        context['message_alert'] = _('Êtes-vous certain de vouloir supprimer l\'étude')
        return context


class StudyList(LoginRequiredMixin, SingleTableView):
    table_class = StudyTable
    model = Study
    template_name = 'table.html'
    table_pagination = {
        'per_page': 25
    }

    def get_context_data(self, **kwargs):
        context = super(StudyList, self).get_context_data(**kwargs)
        context['icon'] = 'fi-page'
        context['title'] = _('Liste des études')
        context['js'] = """
        """
        return context


class TransmitterCreate(LoginRequiredMixin, CreateView):
    """Create view for the Transmitter model."""

    model = Transmitter
    form_class = TransmitterForm
    template_name = 'normal_form.html'

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(TransmitterCreate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(TransmitterCreate, self).get_context_data(**kwargs)
        context['icon'] = 'fa fa-podcast'
        context['title'] = _('Ajout d\'un émetteur')
        context['js'] = js.DateInput
        return context


class TransmitterUpdate(LoginRequiredMixin, UpdateView):
    """Update view for the Transmitter model."""

    model = Transmitter
    form_class = TransmitterForm
    template_name = 'normal_form.html'

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super(TransmitterUpdate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(TransmitterUpdate, self).get_context_data(**kwargs)
        context['icon'] = 'fa fa-podcast'
        context['title'] = _('Modification d\'un émetteur')
        context['js'] = js.DateInput
        return context


class TransmitterDelete(ManagementAuthMixin, DeleteView):
    """Delete view for the Transmitter model."""
    model = Transmitter
    template_name = 'confirm_delete.html'
    success_url = reverse_lazy('management:transmitter_list')

    def get_context_data(self, **kwargs):
        context = super(TransmitterDelete, self).get_context_data(**kwargs)
        context['icon'] = 'fi-trash'
        context['title'] = _('Suppression d\'un émetteur')
        context['message_alert'] = _('Êtes-vous certain de vouloir supprimer l\'émetteur')
        return context


class TransmitterList(LoginRequiredMixin, SingleTableView):
    table_class = TransmitterTable
    model = Transmitter
    template_name = 'table.html'
    table_pagination = {
        'per_page': 25
    }

    def get_context_data(self, **kwargs):
        context = super(TransmitterList, self).get_context_data(**kwargs)
        context['icon'] = 'fa fa-podcast'
        context['title'] = _('Liste des émetteurs')
        context['js'] = """
        """
        return context


class CatchAuthCreate(CatchAuthCreateAuthMixin, CreateView):
    """Update view for the Transmitter model."""

    model = CatchAuth
    form_class = CatchAuthForm
    file_storage = FileSystemStorage()
    template_name = 'normal_form.html'

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(CatchAuthCreate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CatchAuthCreate, self).get_context_data(**kwargs)
        context['icon'] = 'fi-checkbox'
        context['title'] = _('Ajout d\'un arrêté d\'autorisation')
        context['js'] = js.DateInput
        return context


class CatchAuthUpdate(ManagementAuthMixin, UpdateView):
    """Update view for the Transmitter model."""

    model = CatchAuth
    form_class = CatchAuthForm
    file_storage = FileSystemStorage()
    template_name = 'normal_form.html'

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        if form.instance.date_start > form.instance.date_end:
            messages.error(self.request, _('Cette session existe déjà'))
            return HttpResponseRedirect(self.request.path)
        return super(CatchAuthUpdate, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(CatchAuthUpdate, self).get_context_data(**kwargs)
        context['icon'] = 'fi-checkbox'
        context['title'] = _('Modification d\'un arrêté d\'autorisation')
        context['js'] = js.DateInput
        return context


class CatchAuthDelete(ManagementAuthMixin, DeleteView):
    """Delete view for the Transmitter model."""
    model = CatchAuth
    template_name = 'confirm_delete.html'
    success_url = reverse_lazy('management:catchauth_list')

    def get_context_data(self, **kwargs):
        context = super(CatchAuthDelete, self).get_context_data(**kwargs)
        context['icon'] = 'fi-trash'
        context['title'] = _('Suppression d\'un arrêté d\'autorisation')
        context['message_alert'] = _('Êtes-vous certain de vouloir supprimer l\'arrêté')
        return context


class CatchAuthList(LoginRequiredMixin, SingleTableView):
    table_class = CatchAuthTable
    model = CatchAuth
    template_name = 'table.html'
    table_pagination = {
        'per_page': 25
    }

    def get_context_data(self, **kwargs):
        context = super(CatchAuthList, self).get_context_data(**kwargs)
        context['icon'] = 'fi-checkbox'
        context['title'] = _('Liste des arrêtés d\'autorisation')
        context['js'] = """
        """
        return context
