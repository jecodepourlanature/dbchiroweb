from django.conf.urls import url

from .views import UserCreate, UserUpdate, UserPassword, UserDelete, change_password, UserListView, UserDetail, MyProfileUpdate, \
    MyProfileDetail

# from django.contrib.auth.urls import views

urlpatterns = [
    url(r'^create/$', UserCreate.as_view(), name='user_create'),
    url(r'^(?P<pk>[0-9]+)/update$', UserUpdate.as_view(), name='user_update'),
    url(r'^(?P<pk>[0-9]+)/password$', UserPassword.as_view(), name='user_password'),
    url(r'^(?P<pk>[0-9]+)/delete$', UserDelete.as_view(), name='user_delete'),
    url(r'^(?P<pk>[0-9]+)/detail$', UserDetail.as_view(), name='user_detail'),
    url(r'^myprofile/change_password$', change_password, name='change_password'),
    url(r'^myprofile/detail$', MyProfileDetail.as_view(), name='myprofile_detail'),
    url(r'^myprofile/update$', MyProfileUpdate.as_view(), name='myprofile_update'),
    url(r'^list/$', UserListView.as_view(), name='user_search'),
]
