from crispy_forms.helper import FormHelper
from crispy_forms.bootstrap import AccordionGroup

def input_sm_helper(form):
    """ set all bootstrap input as input-sm """
    form.helper = FormHelper(form)
    form.helper.field_class = 'input-sm'

class PrimaryAccordionGroup(AccordionGroup):
    template = 'crispy/accordion-group-primary.html'

class InfoAccordionGroup(AccordionGroup):
    template = 'crispy/accordion-group-info.html'

class DangerAccordionGroup(AccordionGroup):
    template = 'crispy/accordion-group-danger.html'