from django.conf.urls import url

from .views import (UnauthorizedModify, UnauthorizedView)

urlpatterns = [
    # url d'erreurs
    url(r'^unauthorizedmodify', UnauthorizedModify.as_view(), name='update_unauth'),
    url(r'^unauthorizedview', UnauthorizedView.as_view(), name='view_unauth'),
]
