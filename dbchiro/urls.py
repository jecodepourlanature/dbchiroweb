"""dbchiro URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url, include, static
from django.contrib.auth import views as auth_views
from django.contrib.auth.urls import views
from django.contrib.gis import admin

admin.autodiscover()
admin.site.index_title = 'Tables de données'
admin.site.site_header = 'Interface de gestion'

urlpatterns = [
                  url(r'^nested_admin/', include('nested_admin.urls')),
                  url(r'^login/$', auth_views.LoginView.as_view(template_name='login.html'), name='login'),
                  url(r'^logout/$', auth_views.LogoutView.as_view(template_name='logout.html'), name='logout'),
                  url(r'^password_reset/$', views.PasswordResetView.as_view(), name='password_reset'),
                  url(r'^password_reset/done/$', views.PasswordResetDoneView.as_view(), name='password_reset_done'),
                  url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
                      views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
                  url(r'^reset/done/$', views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),
                  url(r'^admin/', admin.site.urls),
                  url(r'^admin/doc/', include('django.contrib.admindocs.urls'),
                      name='dbgestion_doc'),
                  url(r'', include('core.urls', namespace='core')),
                  url(r'^accounts/', include('accounts.urls', namespace='accounts')),
                  url(r'', include('blog.urls', namespace='blog')),
                  url(r'', include('sights.urls', namespace='sights')),
                  url(r'', include('management.urls', namespace='management')),
                  url(r'^synthesis/', include('synthesis.urls', namespace='synthesis')),
                  url(r'^api/', include('api.urls', namespace='api')),

              ] + static.static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
                      url(r'^__debug__/', include(debug_toolbar.urls)),
                  ] + urlpatterns
