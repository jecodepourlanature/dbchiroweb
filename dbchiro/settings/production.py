from dbchiro.settings.base import *

DEBUG=False

INSTALLED_APPS = MAIN_APPS + PROJECT_APPS

# Paramètres pour l'utilisation de la navigation sécurisée par https://
SECURE_SSL_REDIRECT=True
SESSION_COOKIE_SECURE=True
CSRF_COOKIE_SECURE=True
