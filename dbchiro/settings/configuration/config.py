import os
"""FICHIER DE CONFIGURATION DE DBCHIROWEB"""


# Nom du site, affiché sur les pages internet.
SITE_NAME = 'dbChiro[web]'

# Hôtes de cette instance de dbChiro (liste python, entre crochets), si plusieurs, ajouter comme tel ['domaine1.org','domaine2.net','.domaine3.net']
# https://docs.djangoproject.com/fr/1.11/ref/settings/#std:setting-ALLOWED_HOSTS
CUSTOM_HOSTS = ['.dbchiro.org','.herokuapp.com']

# Clé secrète, peut être générée ici :
# https://www.miniwebtool.com/django-secret-key-generator/
SECRET_KEY = 'sc&@y&%c+czi)j8e*z!8alj-u7%b8m06k1pby&)hr^xf2a4q_%'

# Paramètres de la base de donnée PostgreSQL, l'utilisateur doit être superutilisateur
DB_HOST = os.environ['DATABASE_HOST'] # Hôte de la bdd
DB_PORT = 5432 # Port de la bdd
DB_NAME = os.environ['DATABASE_NAME'] # Nom de la bdd
DB_USER = os.environ['DATABASE_USER'] # utilisateur de la bdd
DB_PWD = os.environ['DATABASE_PASSWORD'] # Mot de passe de la bdd

# Paramètres du compte email pour la transmission d'emails
EMAIL_SMTP = 'ssl0.ovh.net' # pour ovh
EMAIL_SSL = True # pour ovh
EMAIL_PORT = 465 # pour ovh
EMAIL_USER = 'user@domain.tld'
EMAIL_PWD = 'mdp'
EMAIL_DEFAULT_FROM = 'user@domain.tld'
EMAIL_SUBJECT_PREFIX = '[dbChiro] '

# Paramètres cartographiques et géographiques
# Projection par défaut des tables de données cartographiques du projet
GEO_PROJECTION = 4326
# Latitude et longitude du centroid du territoire couvert par dbChiro[web]
MAP_Y = 45
MAP_X = 5
# Niveau de zoom par défaut des cartes LeafLet (pour afficher tout le territoire)
MAP_DEFAULT_ZOOM = 8
# Clé permettant d'accéder aux couches de l'IGN via leur API
# A commander ici (une version de base est gratuite pour un usage grand public limité): http://professionnels.ign.fr/ign/contrats
IGN_API_KEY = 'yourapikey'
# Clé permettant d'accéder à des couches MapBox
# https://www.mapbox.com/maps/
MAPBOX_API_KEY = 'yourapikey'
# Altitude mini et maxi possible à la saisie des localités.
ALTITUDE_MAX = 4800
ALTITUDE_MIN = -20

#  Cycle annuel des chiroptères pour l'attribution auto de la période à une observation

# doy                   | 335	    | 60	    | 136	    | 228
# date                  | 01/12     | 01/03	    | 16/05	    | 16/08
# ----------------------+-----------+-----------+-----------+----------
# wintering (w)         |     ≥	    |     <     |           |
# sping transit (st)    |   	    |     ≥	    |     <     |
# Summering (e)		    |	        |           |     ≥	    |    <
# Automn transit (ta)	|     <     |           |     ≥     |

PERIOD_WINTERING_START = 335
PERIOD_WINTERING_VALUE = 'Hivernant'
PERIOD_SPRING_TRANSIT_START = 60
PERIOD_SPRING_TRANSIT_VALUE = 'Transit printanier'
PERIOD_START_SUMMERING_START = 136
PERIOD_START_SUMMERING_VALUE = 'Estivage'
PERIOD_START_AUTUMN_TRANSIT_START = 228
PERIOD_START_AUTUMN_TRANSIT_VALUE = 'Transit automnal'
